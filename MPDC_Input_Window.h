/*
 * MPDClient - A Music Player Daemon client
 * Copyright (C) 2025 Xander

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>

class MPDC_Input_Window: public Fl_Double_Window {
    Fl_Box *action_text_box,
           *action_separator_box,
           *action_buttons_no_yes_box,
           *button_no_box,
           *button_yes_box;

    Fl_Input *action_input;

    Fl_Button *button_no;
    Fl_Button *button_yes;

protected:
    // Static shit
    static void s_action_input_changed(Fl_Widget *, void *data) {
        ((MPDC_Input_Window *)data)->action_input_changed();
    }

    static void s_done(Fl_Widget *, void *data) {
        ((MPDC_Input_Window *)data)->done();
    }

    // Normal callbacks
    void action_input_changed(void) {
        if (action_input->size() == 0)
            button_yes->deactivate();
        else
            button_yes->activate();

        button_yes->redraw();
    }

    void done(void) {
        hide();
    }

public:
    // Constructor
    MPDC_Input_Window(int w, int h, const char *title): Fl_Double_Window(w, h, title) {
        Fl::visual(FL_DOUBLE | FL_INDEX);

        // Input
        Fl_Group *group_action_text = new Fl_Group(
            0,
            0,
            w,
            h / 3,
            NULL
        );

        action_input = new Fl_Input(
            group_action_text->x() + 4,
            group_action_text->y() + 4,
            group_action_text->w() - 8,
            group_action_text->h() - 8,
            NULL
        );
        action_input->color(FL_BACKGROUND2_COLOR);
        action_input->when(FL_WHEN_CHANGED);
        action_input->callback(s_action_input_changed, (void *)this);

        group_action_text->resizable(action_input);
        group_action_text->end();

        // Separator box
        action_separator_box = new Fl_Box(
            FL_NO_BOX,
            0,
            0 + group_action_text->h(),
            w,
            h / 3,
            NULL
        );

        // Press yes or no
        Fl_Group *group_action_buttons_no_yes = new Fl_Group(
            0,
            action_separator_box->y() + action_separator_box->h(),
            w,
            h - group_action_text->h() - action_separator_box->h(),
            NULL
        );

        // No
        button_no = new Fl_Button(
            group_action_buttons_no_yes->x() + 4,
            group_action_buttons_no_yes->y() + 4,
            28 * 3,
            group_action_buttons_no_yes->h() - 8,
            NULL
        );
        button_no->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        button_no->callback(s_done, (void *)this);

        // Separator
        Fl_Box *action_buttons_separator_box = new Fl_Box(
            FL_NO_BOX,
            button_no->x() + button_no->w() + 4,
            group_action_buttons_no_yes->y(),
            group_action_buttons_no_yes->w() - 28 * 6 - 8 * 2,
            group_action_buttons_no_yes->h(),
            NULL
        );

        // Yes
        button_yes = new Fl_Button(
            action_buttons_separator_box->x() + action_buttons_separator_box->w() + 4,
            group_action_buttons_no_yes->y() + 4,
            28 * 3,
            group_action_buttons_no_yes->h() - 8,
            NULL
        );
        button_yes->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        button_yes->type(FL_TOGGLE_BUTTON);
        button_yes->callback(s_done, (void *)this);
        button_yes->deactivate();

        group_action_buttons_no_yes->resizable(action_buttons_separator_box);
        group_action_buttons_no_yes->end();

        // End
        resizable(action_separator_box);
        end();
        set_modal();
        show();
    }

    // get text
    const char *text(void) const {
        return action_input->value();
    }

    // Set message
    void action_message(const char *m) {
        action_input->value(m);
    }

    // Enable "yes"
    void enable_yes_button(void) {
        button_yes->activate();
        button_yes->redraw();
    }

    // Get "yes" value
    int value(void) const {
        return button_yes->value();
    }

    // Set buttons' labels
    void button_no_label(const char *l) {
        button_no->label(l);
    }

    void button_yes_label(const char *l) {
        button_yes->label(l);
    }

    // Set font
    void textfont(Fl_Font ft) {
        action_input->textfont(ft);

        button_no->labelfont(ft);
        button_yes->labelfont(ft);
    }

    // Set font size
    void textsize(Fl_Fontsize sz) {
        action_input->textsize(sz);

        button_no->labelsize(sz);
        button_yes->labelsize(sz);
    }

    // Destroy
    ~MPDC_Input_Window() {
    }
};
