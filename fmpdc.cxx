/*
 * FLTK MPD Client (aka MPDClient) - A Music Player Daemon client
 * Copyright (C) 2025 Xander

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Pixmap.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Toggle_Button.H>
#include <FL/Fl_Text_Editor.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Wizard.H>
#include <FL/Fl_Tooltip.H>
#include <FL/Fl_Shared_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_SVG_Image.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Menu_Button.H>
#include <FL/Fl_Value_Input.H>
#include <FL/filename.H>
#include <mpd/client.h>
#include <libgen.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#include "MPDC_Input_Window.h"
#include "MPDC_Confirm_Window.h"
#include "MPDC_Info_Window.h"
#include "MPDC_Choice.h"
#include "MPDC_Browser.h"
#include "MPDC_Prog_Bar.h"
#include "lc.h"
#include "icons/mpdclient.xpm"
#include "icons/backward_big.xpm"
#include "icons/forward_big.xpm"
#include "icons/play_big.xpm"
#include "icons/pause_big.xpm"
#include "icons/paused_big.xpm"
#include "icons/stop_big.xpm"
#include "icons/save_big.xpm"
#include "icons/clear_big.xpm"
#include "icons/delete_big.xpm"
#include "icons/search.xpm"
#include "icons/rename_big.xpm"
#include "icons/crossfade_big.xpm"
#include "icons/crossfade_on_big.xpm"
#include "icons/shuffle_big.xpm"
#include "icons/repeat_big.xpm"
#include "icons/repeat_on_big.xpm"
#include "icons/random_big.xpm"
#include "icons/random_on_big.xpm"
#include "icons/update.xpm"

#define APP_NAME "FMPDC"
#define APP_VERSION "5.1"

// UI GLOBALS
Fl_Double_Window *main_window;

Fl_Toggle_Button *bt_cover_art,
                 *bt_artists,
                 *bt_genres,
                 *bt_lists,
                 *bt_lyrics,
                 *bt_queue;

Fl_Button *button_pause,
          *button_crossfade,
          *button_random,
          *button_repeat,
          *button_save,
          *button_rename,
          *button_delete,
          *button_search,
          *button_update;

Fl_Wizard *wizard;

Fl_Group *artwork_tab,
         *artists_tab,
         *genres_tab,
         *lists_tab,
         *lyrics_tab,
         *queue_tab;

Fl_Browser *artists_browser,
           *albums_browser,
           *songs_browser,
           *genres_albums_browser,
           *genres_songs_browser,
           *lists_browser,
           *list_songs_browser;

MPDC_Browser *queue_browser;

Fl_Box *images_box,
       *logo_box,
       *artist_box,
       *cover_box;

Fl_Output *info_output;

MPDC_Prog_Bar *prog_bar;

Fl_Image *px_forward,
         *px_backward,
         *px_play,
         *px_pause,
         *px_paused,
         *px_stop,
         *px_save,
         *px_save_inac,
         *px_clear,
         *px_rename,
         *px_rename_inac,
         *px_crossfade,
         *px_crossfade_on,
         *px_shuffle,
         *px_repeat,
         *px_repeat_on,
         *px_random,
         *px_random_on,
         *px_delete,
         *px_delete_inac,
         *px_search,
         *px_search_inac,
         *px_update,
         *px_update_inac,
         *px_mpdclient;

Fl_Input *search_input;

MPDC_Choice *chosen_genre_1,
            *chosen_genre_2,
            *chosen_genre_3;

Fl_Text_Editor *lyrics_viewer;

Fl_Text_Buffer *lyrics_buffer;

Fl_Value_Input *input_crossfade;

// OPTIONS
const char *scheme = "none";
const char *music_directory = NULL;
const char *host = "localhost";
int help_flag = 0;
int port = 6600;
int crossfade_sec = 0;
int font_size = FL_NORMAL_SIZE;
bool display_images = true;
bool compact = false;
bool own_icons = false;

// MPD CONNECTION
struct mpd_connection *connection;

// CROSSFADE STATUS
bool crossfade_on = false;

// RANDOM STATUS
bool random_on = false;

// REPEAT STATUS
bool repeat_on = false;

// CURRENT SONG FULL PATH FOR LYRICS
char full_path[FL_PATH_MAX] = "";

// IF TRUE, POPUP MENU APPEARS
bool user_grabbed_sel = false;

// ICON THEME
char icon_theme[256] = "default";
char icon_theme_orig[256] = "";

// PROTOTYPES
void switch_view(Fl_Widget *, void *);
void monitor(void *);
unsigned get_time_left(unsigned, int, unsigned);
unsigned fill_queue(void);
void get_artists(void);
void get_albums(Fl_Widget *, void *);
void get_songs(Fl_Widget *, void *);
void add_song(Fl_Widget *, void *);
void get_lists(void);
void get_list_songs(Fl_Widget *, void *);
void add_list_song(Fl_Widget *, void *);
void save_list(Fl_Widget *, void *);
void rename_list(Fl_Widget *, void *);
void delete_list(Fl_Widget *, void *);
void get_genres(void);
void get_genres_albums(Fl_Widget *, void *);
void get_genres_songs(Fl_Widget *, void *);
void add_genre_song(Fl_Widget *, void *);
void quit(Fl_Widget *, void *);
int scan_args(int, char **, int &);
bool is_stream(const char *);
void get_images(const char *);
void scale_and_show_images(Fl_Shared_Image *, Fl_Shared_Image *, Fl_Shared_Image *);
void queue_cb(Fl_Widget *, void *);
void play_prev(Fl_Widget *, void *);
void play_next(Fl_Widget *, void *);
void play(Fl_Widget *, void *);
void toggle_pause(Fl_Widget *, void *);
void stop(Fl_Widget *, void *);
void clear_queue(Fl_Widget *, void *);
void shuffle(Fl_Widget *, void *);
void crossfade(Fl_Widget *, void *);
void set_crossfade(Fl_Widget *, void *);
void random(Fl_Widget *, void *);
void repeat(Fl_Widget *, void *);
void move_down(Fl_Widget *, void *);
void move_up(Fl_Widget *, void *);
void remove_item(Fl_Widget *, void *);
void show_lyrics(const char *);
void save_lyrics(int, int, int, int, const char *, void *);
void search(Fl_Widget *, void *);
void *search_thread(void *);
void search_input_check(Fl_Widget *, void *);
void rewind(Fl_Widget *, void *);
void update(Fl_Widget *, void *);
void *update_thread(void *);
void update_thread_end(void *);
void get_icon_theme(void);
Fl_Image *get_icon(const char *, const char *);

// MAIN
int main(int argc, char *argv[]) {
    int i = 1;

    if (Fl::args(argc, argv, i, scan_args) < argc)
        Fl::fatal(
            "error: unknown option: %s\n"
            "usage: %s [options]\n"
            " -h | --help         : print extended help message\n"
            " -S | --scheme     # : set a scheme to use (default: none)\n"
            " -D | --directory  # : set music directory (mandatory)\n"
            " -H | --host       # : set host name (default: localhost)\n"
            " -P | --port       # : set port number (default: 6600)\n"
            " -c | --crossfade  # : set crossfade time in seconds (default: 0)\n"
            " -t | --text-size  # : set text size in pixels (default: 14)\n"
            " -n | --no-images    : do not show images (default: images are shown)\n"
            " -C | --compact      : start in the compact mode (default: no)\n"
            " -o | --own-icons    : use built-in icons (default: no)\n"
            " plus standard fltk options\n",
            argv[i],
            argv[0]
        );

    if (help_flag)
        Fl::fatal(
            "usage: %s [options]\n"
            " -h | --help         : print extended help message\n"
            " -S | --scheme     # : set a scheme to use (default: none)\n"
            " -D | --directory  # : set music directory (mandatory)\n"
            " -H | --host       # : set host name (default: localhost)\n"
            " -P | --port       # : set port number (default: 6600)\n"
            " -c | --crossfade  # : set crossfade time in seconds (default: 0)\n"
            " -t | --text-size  # : set text size in pixels (default: 14)\n"
            " -n | --no-images    : do not show images (default: images are shown)\n"
            " -C | --compact      : start in the compact mode (default: no)\n"
            " -o | --own-icons    : use built-in icons (default: no)\n"
            " plus standard fltk options:\n"
            "%s\n",
            argv[0],
            Fl::help
        );

    Fl::visual(FL_DOUBLE | FL_INDEX);

    Fl::scheme(scheme);

    fl_register_images();

    get_icon_theme();

    do_localize(&lc);

    Fl_Tooltip::delay(0);
    Fl_Tooltip::hoverdelay(0);
    Fl_Tooltip::size(font_size);

    if (own_icons) {
        px_forward = new Fl_Pixmap(forward_big_xpm);
        px_backward = new Fl_Pixmap(backward_big_xpm);
        px_play = new Fl_Pixmap(play_big_xpm);
        px_pause = new Fl_Pixmap(pause_big_xpm);
        px_paused = new Fl_Pixmap(paused_big_xpm);
        px_stop = new Fl_Pixmap(stop_big_xpm);
        px_save = new Fl_Pixmap(save_big_xpm);
        px_save_inac  = (Fl_Pixmap *)px_save->copy();
        px_save_inac->inactive();
        px_clear = new Fl_Pixmap(clear_big_xpm);
        px_delete = new Fl_Pixmap(delete_big_xpm);
        px_delete_inac  = (Fl_Pixmap *)px_delete->copy();
        px_delete_inac->inactive();
        px_rename = new Fl_Pixmap(rename_big_xpm);
        px_rename_inac  = (Fl_Pixmap *)px_rename->copy();
        px_rename_inac->inactive();
        px_crossfade = new Fl_Pixmap(crossfade_big_xpm);
        px_crossfade_on = new Fl_Pixmap(crossfade_on_big_xpm);
        px_shuffle = new Fl_Pixmap(shuffle_big_xpm);
        px_repeat = new Fl_Pixmap(repeat_big_xpm);
        px_repeat_on = new Fl_Pixmap(repeat_on_big_xpm);
        px_random = new Fl_Pixmap(random_big_xpm);
        px_random_on = new Fl_Pixmap(random_on_big_xpm);
        px_search = new Fl_Pixmap(search_xpm);
        px_search_inac  = (Fl_Pixmap *)px_search->copy();
        px_search_inac->inactive();
        px_update = new Fl_Pixmap(update_xpm);
        px_update_inac  = (Fl_Pixmap *)px_update->copy();
        px_update_inac->inactive();
        px_mpdclient = new Fl_Pixmap(mpdclient_xpm);

    } else {
        px_forward = get_icon("actions", "media-skip-forward.");
        px_backward = get_icon("actions", "media-skip-backward.");
        px_play = get_icon("actions", "media-playback-start.");
        px_pause = get_icon("actions", "media-playback-pause.");
        px_paused = get_icon("actions", "media-playback-pause.");
        px_stop = get_icon("actions", "media-playback-stop.");
        px_save = get_icon("actions", "add.");
        px_save_inac  = (Fl_Image *)px_save->copy();
        px_save_inac->inactive();
        px_clear = get_icon("actions", "stock_delete.");
        px_delete = get_icon("actions", "stock_close.");
        px_delete_inac  = (Fl_Image *)px_delete->copy();
        px_delete_inac->inactive();
        px_rename = get_icon("actions", "document-save-as.");
        px_rename_inac  = (Fl_Image *)px_rename->copy();
        px_rename_inac->inactive();
        px_shuffle = get_icon("actions", "up.");
        px_crossfade_on = get_icon("actions", "media-playlist-shuffle.");
        px_crossfade = (Fl_Image *)px_crossfade_on->copy();
        px_crossfade->inactive();
        px_repeat_on = get_icon("actions", "media-playlist-repeat.");
        px_repeat = (Fl_Image *)px_repeat_on->copy();
        px_repeat->inactive();
        px_random_on = get_icon("actions", "stock-refresh.");
        px_random = (Fl_Image *)px_random_on->copy();
        px_random->inactive();
        px_search = get_icon("actions", "system-search.");
        px_search_inac  = (Fl_Image *)px_search->copy();
        px_search_inac->inactive();
        px_update = get_icon("actions", "music-library.");
        px_update_inac  = (Fl_Image *)px_update->copy();
        px_update_inac->inactive();
        px_mpdclient = new Fl_Pixmap(mpdclient_xpm);
    }

    if (compact) {
        // Window
        main_window = new Fl_Double_Window(
            (40 * 4) * 5,
            40 * 20,
            APP_NAME
        );
        main_window->icon(px_mpdclient);
        main_window->begin();

        // Toggle buttons
        Fl_Group *group_toggle_buttons = new Fl_Group(
            main_window->x(),
            main_window->y(),
            main_window->w(),
            40,
            NULL
        );

        // Toggle cover art view
        Fl_Group *group_cover_art_button = new Fl_Group(
            group_toggle_buttons->x(),
            group_toggle_buttons->y(),
            group_toggle_buttons->w() / 6,
            group_toggle_buttons->h(),
            NULL
        );

        bt_cover_art = new Fl_Toggle_Button(
            group_cover_art_button->x() + 4,
            group_cover_art_button->y() + 4,
            group_cover_art_button->w() - 4,
            group_cover_art_button->h() - 8,
            lc.artwork
        );
        bt_cover_art->labelsize(font_size);
        bt_cover_art->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_cover_art->callback(switch_view, (void *)1);
        bt_cover_art->value(1);

        group_cover_art_button->resizable(bt_cover_art);
        group_cover_art_button->end();

        // Toggle artists view
        Fl_Group *group_artists_button = new Fl_Group(
            group_cover_art_button->x() + group_cover_art_button->w(),
            group_toggle_buttons->y(),
            group_toggle_buttons->w() / 6,
            group_toggle_buttons->h(),
            NULL
        );

        bt_artists = new Fl_Toggle_Button(
            group_artists_button->x() + 4,
            group_artists_button->y() + 4,
            group_artists_button->w() - 4,
            group_artists_button->h() - 8,
            lc.artists
        );
        bt_artists->labelsize(font_size);
        bt_artists->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_artists->callback(switch_view, (void *)2);

        group_artists_button->resizable(bt_artists);
        group_artists_button->end();

        // Toggle genres view
        Fl_Group *group_genres_button = new Fl_Group(
            group_artists_button->x() + group_artists_button->w(),
            group_toggle_buttons->y(),
            group_toggle_buttons->w() / 6,
            group_toggle_buttons->h(),
            NULL
        );

        bt_genres = new Fl_Toggle_Button(
            group_genres_button->x() + 4,
            group_genres_button->y() + 4,
            group_genres_button->w() - 4,
            group_genres_button->h() - 8,
            lc.genres
        );
        bt_genres->labelsize(font_size);
        bt_genres->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_genres->callback(switch_view, (void *)3);

        group_genres_button->resizable(bt_genres);
        group_genres_button->end();

        // Toggle lists view
        Fl_Group *group_lists_button = new Fl_Group(
            group_genres_button->x() + group_genres_button->w(),
            group_toggle_buttons->y(),
            group_toggle_buttons->w() / 6,
            group_toggle_buttons->h(),
            NULL
        );

        bt_lists = new Fl_Toggle_Button(
            group_lists_button->x() + 4,
            group_lists_button->y() + 4,
            group_lists_button->w() - 4,
            group_lists_button->h() - 8,
            lc.lists
        );
        bt_lists->labelsize(font_size);
        bt_lists->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_lists->callback(switch_view, (void *)4);

        group_lists_button->resizable(bt_lists);
        group_lists_button->end();

        // Toggle lyrics view
        Fl_Group *group_lyrics_button = new Fl_Group(
            group_lists_button->x() + group_lists_button->w(),
            group_toggle_buttons->y(),
            group_toggle_buttons->w() / 6,
            group_toggle_buttons->h(),
            NULL
        );

        bt_lyrics = new Fl_Toggle_Button(
            group_lyrics_button->x() + 4,
            group_lyrics_button->y() + 4,
            group_lyrics_button->w() - 4,
            group_lyrics_button->h() - 8,
            lc.lyrics
        );
        bt_lyrics->labelsize(font_size);
        bt_lyrics->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_lyrics->callback(switch_view, (void *)5);

        group_lyrics_button->resizable(bt_lyrics);
        group_lyrics_button->end();

        // Toggle queue view
        Fl_Group *group_queue_button = new Fl_Group(
            group_lyrics_button->x() + group_lyrics_button->w(),
            group_toggle_buttons->y(),
            group_toggle_buttons->w() - group_cover_art_button->w() - group_artists_button->w() - group_genres_button->w() - group_lists_button->w() - group_lyrics_button->w(),
            group_toggle_buttons->h(),
            NULL
        );

        bt_queue = new Fl_Toggle_Button(
            group_queue_button->x() + 4,
            group_queue_button->y() + 4,
            group_queue_button->w() - 8,
            group_queue_button->h() - 8,
            lc.queue
        );
        bt_queue->labelsize(font_size);
        bt_queue->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_queue->callback(switch_view, (void *)6);

        group_queue_button->resizable(bt_queue);
        group_queue_button->end();

        group_toggle_buttons->end();

        // Wizard
        wizard = new Fl_Wizard(
            main_window->x(),
            main_window->y() + group_toggle_buttons->h(),
            main_window->w(),
            main_window->h() - 40 * 3
        );
        wizard->box(FL_NO_BOX);

        // Artwork
        artwork_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        images_box = new Fl_Box(
            FL_NO_BOX,
            artwork_tab->x() + 4,
            artwork_tab->y(),
            artwork_tab->w() - 8,
            artwork_tab->h(),
            NULL
        );

        artwork_tab->resizable(images_box);
        artwork_tab->end();

        // Artists
        artists_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *artists_left_pane = new Fl_Group(
            artists_tab->x(),
            artists_tab->y(),
            artists_tab->w() / 3,
            artists_tab->h(),
            NULL
        );

        Fl_Box *artists_left_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            artists_left_pane->x(),
            artists_left_pane->y(),
            artists_left_pane->w(),
            artists_left_pane->h(),
            NULL
        );

        Fl_Box *artists_left_pane_box_inner = new Fl_Box(
            FL_DOWN_BOX,
            artists_left_pane_box_outer->x() + 4,
            artists_left_pane_box_outer->y(),
            artists_left_pane_box_outer->w() - 4,
            artists_left_pane_box_outer->h(),
            NULL
        );

        artists_browser = new Fl_Browser(
            artists_left_pane_box_inner->x() + 2,
            artists_left_pane_box_inner->y() + 2,
            artists_left_pane_box_inner->w() - 4,
            artists_left_pane_box_inner->h() - 44,
            NULL
        );
        artists_browser->textsize(font_size);
        artists_browser->color(FL_BACKGROUND2_COLOR);
        artists_browser->box(FL_FLAT_BOX);
        artists_browser->type(FL_HOLD_BROWSER);
        artists_browser->callback(get_albums);

        // Artists management
        Fl_Group *group_artists_management = new Fl_Group(
            artists_browser->x(),
            artists_browser->y() + artists_browser->h(),
            artists_browser->w(),
            40,
            NULL
        );

        Fl_Box *artists_left_pane_control_box = new Fl_Box(
            FL_UP_BOX,
            group_artists_management->x(),
            group_artists_management->y(),
            group_artists_management->w() - 40,
            group_artists_management->h(),
            NULL
        );

        button_search = new Fl_Button(
            artists_left_pane_control_box->x() + 4,
            artists_left_pane_control_box->y() + 4,
            32,
            artists_left_pane_control_box->h() - 8,
            NULL
        );
        button_search->box(FL_NO_BOX);
        button_search->down_box(FL_DOWN_BOX);
        button_search->image(px_search);
        button_search->deimage(px_search_inac);
        button_search->tooltip(lc.search);
        button_search->callback(search);
        button_search->deactivate();

        search_input = new Fl_Input(
            button_search->x() + button_search->w() + 4,
            artists_left_pane_control_box->y() + 4,
            artists_left_pane_control_box->w() - 32 - 4 * 3,
            artists_left_pane_control_box->h() - 8,
            NULL
        );
        search_input->textsize(font_size);
        search_input->color(FL_BACKGROUND2_COLOR);
        search_input->when(FL_WHEN_CHANGED);
        search_input->callback(search_input_check);

        // Update button
        Fl_Box *button_update_box = new Fl_Box(
            FL_UP_BOX,
            artists_left_pane_control_box->x() + artists_left_pane_control_box->w(),
            artists_left_pane_control_box->y(),
            group_artists_management->w() - artists_left_pane_control_box->w(),
            artists_left_pane_control_box->h(),
            NULL
        );

        button_update = new Fl_Button(
            button_update_box->x() + 4,
            button_update_box->y() + 4,
            button_update_box->w() - 8,
            button_update_box->h() - 8,
            NULL
        );
        button_update->box(FL_NO_BOX);
        button_update->down_box(FL_DOWN_BOX);
        button_update->image(px_update);
        button_update->deimage(px_update_inac);
        button_update->tooltip(lc.update);
        button_update->callback(update);

        group_artists_management->resizable(search_input);
        group_artists_management->end();

        artists_left_pane->resizable(artists_browser);
        artists_left_pane->end();

        // Albums by artist
        Fl_Group *artists_middle_pane = new Fl_Group(
            artists_left_pane->x() + artists_left_pane->w(),
            artists_left_pane->y(),
            artists_tab->w() / 3,
            artists_left_pane->h(),
            NULL
        );

        Fl_Box *artists_middle_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            artists_middle_pane->x(),
            artists_middle_pane->y(),
            artists_middle_pane->w(),
            artists_middle_pane->h(),
            NULL
        );

        int albums_browser_widths[] = {50, 350, 0};

        albums_browser = new Fl_Browser(
            artists_middle_pane_box_outer->x() + 4,
            artists_middle_pane_box_outer->y(),
            artists_middle_pane_box_outer->w() - 4,
            artists_middle_pane_box_outer->h(),
            NULL
        );
        albums_browser->textsize(font_size);
        albums_browser->color(FL_BACKGROUND2_COLOR);
        albums_browser->type(FL_HOLD_BROWSER);
        albums_browser->column_widths(albums_browser_widths);
        albums_browser->column_char('\t');
        albums_browser->callback(get_songs);

        artists_middle_pane->resizable(albums_browser);
        artists_middle_pane->end();

        // Songs by album
        Fl_Group *artists_right_pane = new Fl_Group(
            artists_middle_pane->x() + artists_middle_pane->w(),
            artists_middle_pane->y(),
            artists_tab->w() - artists_left_pane->w() - artists_middle_pane->w(),
            artists_middle_pane->h(),
            NULL
        );

        Fl_Box *artists_right_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            artists_right_pane->x(),
            artists_right_pane->y(),
            artists_right_pane->w(),
            artists_right_pane->h(),
            NULL
        );

        int songs_browser_widths[] = {30, 350, 0};

        songs_browser = new Fl_Browser(
            artists_right_pane_box_outer->x() + 4,
            artists_right_pane_box_outer->y(),
            artists_right_pane_box_outer->w() - 8,
            artists_right_pane_box_outer->h(),
            NULL
        );
        songs_browser->textsize(font_size);
        songs_browser->color(FL_BACKGROUND2_COLOR);
        songs_browser->type(FL_HOLD_BROWSER);
        songs_browser->column_widths(songs_browser_widths);
        songs_browser->column_char('\t');
        songs_browser->callback(add_song);

        artists_right_pane->resizable(songs_browser);
        artists_right_pane->end();

        artists_tab->end();

        // Genres
        genres_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *genres_left_pane = new Fl_Group(
            genres_tab->x(),
            genres_tab->y(),
            genres_tab->w() / 2,
            genres_tab->h(),
            NULL
        );

        Fl_Box *genres_left_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            genres_left_pane->x(),
            genres_left_pane->y(),
            genres_left_pane->w(),
            genres_left_pane->h(),
            NULL
        );

        Fl_Box *genres_left_pane_box_inner = new Fl_Box(
            FL_DOWN_BOX,
            genres_left_pane_box_outer->x() + 4,
            genres_left_pane_box_outer->y(),
            genres_left_pane_box_outer->w() - 4,
            genres_left_pane_box_outer->h(),
            NULL
        );

        int genres_albums_browser_widths[] = {50, 350, 0};

        genres_albums_browser = new Fl_Browser(
            genres_left_pane_box_inner->x() + 2,
            genres_left_pane_box_inner->y() + 2,
            genres_left_pane_box_inner->w() - 4,
            genres_left_pane_box_inner->h() - 44,
            NULL
        );
        genres_albums_browser->textsize(font_size);
        genres_albums_browser->color(FL_BACKGROUND2_COLOR);
        genres_albums_browser->box(FL_FLAT_BOX);
        genres_albums_browser->type(FL_HOLD_BROWSER);
        genres_albums_browser->column_widths(genres_albums_browser_widths);
        genres_albums_browser->column_char('\t');
        genres_albums_browser->callback(get_genres_songs);

        // Genres management
        Fl_Group *group_genres_management = new Fl_Group(
            genres_albums_browser->x(),
            genres_albums_browser->y() + genres_albums_browser->h(),
            genres_albums_browser->w(),
            40,
            NULL
        );

        Fl_Box *genres_control_box = new Fl_Box(
            FL_UP_BOX,
            group_genres_management->x(),
            group_genres_management->y(),
            group_genres_management->w(),
            group_genres_management->h(),
            NULL
        );

        // One
        Fl_Group *group_genre_1 = new Fl_Group(
            genres_control_box->x(),
            genres_control_box->y(),
            genres_control_box->w() / 3,
            genres_control_box->h(),
            NULL
        );

        Fl_Box *genre_box_1 = new Fl_Box(
            FL_NO_BOX,
            group_genre_1->x(),
            group_genre_1->y(),
            group_genre_1->w(),
            group_genre_1->h(),
            NULL
        );

        chosen_genre_1 = new MPDC_Choice(
            genre_box_1->x() + 4,
            genre_box_1->y() + 4,
            genre_box_1->w() - 4,
            genre_box_1->h() - 8,
            NULL
        );
        chosen_genre_1->textsize(font_size);
        chosen_genre_1->callback(get_genres_albums);

        group_genre_1->resizable(chosen_genre_1);
        group_genre_1->end();

        // Two
        Fl_Group *group_genre_2 = new Fl_Group(
            group_genre_1->x() + group_genre_1->w(),
            genres_control_box->y(),
            genres_control_box->w() / 3,
            genres_control_box->h(),
            NULL
        );

        Fl_Box *genre_box_2 = new Fl_Box(
            FL_NO_BOX,
            group_genre_2->x(),
            group_genre_2->y(),
            group_genre_2->w(),
            group_genre_2->h(),
            NULL
        );

        chosen_genre_2 = new MPDC_Choice(
            genre_box_2->x() + 4,
            genre_box_2->y() + 4,
            genre_box_2->w() - 4,
            genre_box_2->h() - 8,
            NULL
        );
        chosen_genre_2->textsize(font_size);
        chosen_genre_2->callback(get_genres_albums);

        group_genre_2->resizable(chosen_genre_2);
        group_genre_2->end();

        // Three
        Fl_Group *group_genre_3 = new Fl_Group(
            group_genre_2->x() + group_genre_2->w(),
            genres_control_box->y(),
            genres_control_box->w() - group_genre_1->w() - group_genre_2->w(),
            genres_control_box->h(),
            NULL
        );

        Fl_Box *genre_box_3 = new Fl_Box(
            FL_NO_BOX,
            group_genre_3->x(),
            group_genre_3->y(),
            group_genre_3->w(),
            group_genre_3->h(),
            NULL
        );

        chosen_genre_3 = new MPDC_Choice(
            genre_box_3->x() + 4,
            genre_box_3->y() + 4,
            genre_box_3->w() - 8,
            genre_box_3->h() - 8,
            NULL
        );
        chosen_genre_3->textsize(font_size);
        chosen_genre_3->callback(get_genres_albums);

        group_genre_3->resizable(chosen_genre_3);
        group_genre_3->end();

        group_genres_management->end();

        genres_left_pane->resizable(genres_albums_browser);
        genres_left_pane->end();

        // Songs by genre
        Fl_Group *genres_right_pane = new Fl_Group(
            genres_left_pane->x() + genres_left_pane->w(),
            genres_tab->y(),
            genres_tab->w() - genres_left_pane->w(),
            genres_tab->h(),
            NULL
        );

        Fl_Box *genres_right_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            genres_right_pane->x(),
            genres_right_pane->y(),
            genres_right_pane->w(),
            genres_right_pane->h(),
            NULL
        );

        genres_songs_browser = new Fl_Browser(
            genres_right_pane_box_outer->x() + 4,
            genres_right_pane_box_outer->y(),
            genres_right_pane_box_outer->w() - 8,
            genres_right_pane_box_outer->h(),
            NULL
        );
        genres_songs_browser->textsize(font_size);
        genres_songs_browser->color(FL_BACKGROUND2_COLOR);
        genres_songs_browser->type(FL_HOLD_BROWSER);
        genres_songs_browser->column_widths(songs_browser_widths);
        genres_songs_browser->column_char('\t');
        genres_songs_browser->callback(add_genre_song);

        genres_right_pane->resizable(genres_songs_browser);
        genres_right_pane->end();

        genres_tab->end();

        // Lists
        lists_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *lists_left_pane = new Fl_Group(
            lists_tab->x(),
            lists_tab->y(),
            lists_tab->w() / 2,
            lists_tab->h(),
            NULL
        );

        Fl_Box *lists_left_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            lists_left_pane->x(),
            lists_left_pane->y(),
            lists_left_pane->w(),
            lists_left_pane->h(),
            NULL
        );

        Fl_Box *lists_left_pane_box_inner = new Fl_Box(
            FL_DOWN_BOX,
            lists_left_pane_box_outer->x() + 4,
            lists_left_pane_box_outer->y(),
            lists_left_pane_box_outer->w() - 4,
            lists_left_pane_box_outer->h(),
            NULL
        );
        lists_left_pane_box_inner->color(FL_BACKGROUND2_COLOR);

        lists_browser = new Fl_Browser(
            lists_left_pane_box_inner->x() + 2,
            lists_left_pane_box_inner->y() + 2,
            lists_left_pane_box_inner->w() - 4,
            lists_left_pane_box_inner->h() - 44,
            NULL
        );
        lists_browser->textsize(font_size);
        lists_browser->color(FL_BACKGROUND2_COLOR);
        lists_browser->box(FL_FLAT_BOX);
        lists_browser->type(FL_HOLD_BROWSER);
        lists_browser->callback(get_list_songs);

        // List management
        Fl_Group *group_list_management = new Fl_Group(
            lists_browser->x(),
            lists_browser->y() + lists_browser->h(),
            lists_browser->w(),
            40,
            NULL
        );

        Fl_Box *lists_control_box = new Fl_Box(
            FL_UP_BOX,
            group_list_management->x(),
            group_list_management->y(),
            group_list_management->w(),
            group_list_management->h(),
            NULL
        );

        button_rename = new Fl_Button(
            lists_control_box->x() + 4,
            lists_control_box->y() + 4,
            32,
            lists_control_box->h() - 8,
            NULL
        );
        button_rename->box(FL_NO_BOX);
        button_rename->down_box(FL_DOWN_BOX);
        button_rename->image(px_rename);
        button_rename->deimage(px_rename_inac);
        button_rename->tooltip(lc.ren);
        button_rename->callback(rename_list);
        button_rename->deactivate();

        Fl_Box *lists_hidden_box = new Fl_Box(
            FL_NO_BOX,
            button_rename->x() + button_rename->w() + 4,
            lists_control_box->y() + 4,
            lists_control_box->w() - 32 * 2 - 4 * 4,
            lists_control_box->h() - 8,
            NULL
        );

        button_delete = new Fl_Button(
            lists_hidden_box->x() + lists_hidden_box->w() + 4,
            lists_control_box->y() + 4,
            32,
            lists_control_box->h() - 8,
            NULL
        );
        button_delete->box(FL_NO_BOX);
        button_delete->down_box(FL_DOWN_BOX);
        button_delete->image(px_delete);
        button_delete->deimage(px_delete_inac);
        button_delete->tooltip(lc.del);
        button_delete->callback(delete_list);
        button_delete->deactivate();

        group_list_management->resizable(lists_hidden_box);
        group_list_management->end();

        lists_left_pane->resizable(lists_browser);
        lists_left_pane->end();

        // Songs by list
        Fl_Group *lists_right_pane = new Fl_Group(
            lists_tab->x() + lists_left_pane->w(),
            lists_tab->y(),
            lists_tab->w() - lists_left_pane->w(),
            lists_tab->h(),
            NULL
        );

        Fl_Box *lists_right_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            lists_right_pane->x(),
            lists_right_pane->y(),
            lists_right_pane->w(),
            lists_right_pane->h(),
            NULL
        );

        list_songs_browser = new Fl_Browser(
            lists_right_pane_box_outer->x() + 4,
            lists_right_pane_box_outer->y(),
            lists_right_pane_box_outer->w() - 8,
            lists_right_pane_box_outer->h(),
            NULL
        );
        list_songs_browser->textsize(font_size);
        list_songs_browser->color(FL_BACKGROUND2_COLOR);
        list_songs_browser->type(FL_HOLD_BROWSER);
        list_songs_browser->callback(add_list_song);

        lists_right_pane->resizable(list_songs_browser);
        lists_right_pane->end();

        lists_tab->end();

        // Lyrics
        lyrics_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *lyrics_pane = new Fl_Group(
            lyrics_tab->x(),
            lyrics_tab->y(),
            lyrics_tab->w(),
            lyrics_tab->h(),
            NULL
        );

        Fl_Box *lyrics_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            lyrics_pane->x(),
            lyrics_pane->y(),
            lyrics_pane->w(),
            lyrics_pane->h(),
            NULL
        );

        lyrics_buffer = new Fl_Text_Buffer();
        lyrics_buffer->add_modify_callback(save_lyrics, NULL);

        lyrics_viewer = new Fl_Text_Editor(
            lyrics_pane_box_outer->x() + 4,
            lyrics_pane_box_outer->y(),
            lyrics_pane_box_outer->w() - 8,
            lyrics_pane_box_outer->h(),
            NULL
        );
        lyrics_viewer->textsize(font_size);
        lyrics_viewer->color(FL_BACKGROUND2_COLOR);
        lyrics_viewer->wrap_mode(Fl_Text_Display::WRAP_AT_BOUNDS, 0);

        lyrics_pane->resizable(lyrics_viewer);
        lyrics_pane->end();

        lyrics_tab->end();

        // Queue
        queue_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        int widths[] = {30, 265, 75, 85, 200, 200, 0};

        const char *headers[6] = {lc.track, lc.title, lc.duration, lc.year, lc.artist, lc.album};

        queue_browser = new MPDC_Browser(
            queue_tab->x() + 4,
            queue_tab->y(),
            queue_tab->w() - 8,
            queue_tab->h(),
            NULL
        );
        queue_browser->text_size(font_size);
        queue_browser->column_widths(widths);
        queue_browser->column_char('\t');
        queue_browser->total_columns(6);
        queue_browser->column_headers(headers);
        queue_browser->browser->callback(queue_cb);

        queue_tab->resizable(queue_browser);
        queue_tab->end();

        wizard->end();

        // Playback control stuff
        Fl_Group *group_playback_control = new Fl_Group(
            main_window->x(),
            main_window->y() + 40 + wizard->h(),
            main_window->w(),
            40,
            NULL
        );

        Fl_Button *bt_bw = new Fl_Button(
            group_playback_control->x() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_bw->box(FL_FLAT_BOX);
        bt_bw->down_box(FL_DOWN_BOX);
        bt_bw->image(px_backward);
        bt_bw->tooltip(lc.prev);
        bt_bw->callback(play_prev);

        Fl_Button *bt_fw = new Fl_Button(
            bt_bw->x() + bt_bw->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_fw->box(FL_FLAT_BOX);
        bt_fw->down_box(FL_DOWN_BOX);
        bt_fw->image(px_forward);
        bt_fw->tooltip(lc.next);
        bt_fw->callback(play_next);

        Fl_Button *bt_pl = new Fl_Button(
            bt_fw->x() + bt_fw->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_pl->box(FL_FLAT_BOX);
        bt_pl->down_box(FL_DOWN_BOX);
        bt_pl->image(px_play);
        bt_pl->tooltip(lc.play);
        bt_pl->callback(play);

        button_pause = new Fl_Button(
            bt_pl->x() + bt_pl->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_pause->box(FL_FLAT_BOX);
        button_pause->down_box(FL_DOWN_BOX);
        button_pause->image(px_pause);
        button_pause->tooltip(lc.pause);
        button_pause->callback(toggle_pause);

        Fl_Button *bt_st = new Fl_Button(
            button_pause->x() + button_pause->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_st->box(FL_FLAT_BOX);
        bt_st->down_box(FL_DOWN_BOX);
        bt_st->image(px_stop);
        bt_st->tooltip(lc.stop);
        bt_st->callback(stop);

        Fl_Button *bt_sh = new Fl_Button(
            bt_st->x() + bt_st->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_sh->box(FL_FLAT_BOX);
        bt_sh->down_box(FL_DOWN_BOX);
        bt_sh->image(px_shuffle);
        bt_sh->tooltip(lc.shuffle);
        bt_sh->callback(shuffle);

        button_random = new Fl_Button(
            bt_sh->x() + bt_sh->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_random->box(FL_FLAT_BOX);
        button_random->down_box(FL_DOWN_BOX);
        button_random->image(px_random);
        button_random->tooltip(lc.random);
        button_random->callback(random);

        button_repeat = new Fl_Button(
            button_random->x() + button_random->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_repeat->box(FL_FLAT_BOX);
        button_repeat->down_box(FL_DOWN_BOX);
        button_repeat->image(px_repeat);
        button_repeat->tooltip(lc.repeat);
        button_repeat->callback(repeat);

        button_crossfade = new Fl_Button(
            button_repeat->x() + button_repeat->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_crossfade->box(FL_FLAT_BOX);
        button_crossfade->down_box(FL_DOWN_BOX);
        button_crossfade->image(px_crossfade);
        button_crossfade->tooltip(lc.crossfade);
        button_crossfade->callback(crossfade);

        input_crossfade = new Fl_Value_Input(
            button_crossfade->x() + button_crossfade->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        input_crossfade->textsize(font_size);
        input_crossfade->color(FL_BACKGROUND2_COLOR);
        input_crossfade->tooltip(lc.crossfade);
        input_crossfade->soft();
        input_crossfade->minimum(0);
        input_crossfade->value(crossfade_sec);
        input_crossfade->callback(set_crossfade);

        Fl_Button *bt_cl = new Fl_Button(
            group_playback_control->w() - 4 - 32,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_cl->box(FL_FLAT_BOX);
        bt_cl->down_box(FL_DOWN_BOX);
        bt_cl->image(px_clear);
        bt_cl->tooltip(lc.clear);
        bt_cl->callback(clear_queue);

        button_save = new Fl_Button(
            bt_cl->x() - 4 - 32,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_save->box(FL_FLAT_BOX);
        button_save->down_box(FL_DOWN_BOX);
        button_save->image(px_save);
        button_save->deimage(px_save_inac);
        button_save->tooltip(lc.save);
        button_save->callback(save_list);

        // Progress bar
        prog_bar = new MPDC_Prog_Bar(
            input_crossfade->x() + input_crossfade->w() + 4,
            group_playback_control->y() + 4,
            group_playback_control->w() - 32 * 12 - 4 * 14,
            group_playback_control->h() - 8,
            NULL
        );
        prog_bar->minimum(0);
        prog_bar->maximum(1);
        prog_bar->callback(rewind);
        prog_bar->timetip->textsize(font_size);

        group_playback_control->resizable(prog_bar);
        group_playback_control->end();

        // Info bar
        Fl_Group *info_group = new Fl_Group(
            group_playback_control->x(),
            group_playback_control->y() + group_playback_control->h(),
            group_playback_control->w(),
            40
        );

        info_output = new Fl_Output(
            info_group->x() + 4,
            info_group->y(),
            info_group->w() - 8,
            info_group->h() - 4,
            NULL
        );
        info_output->textsize(font_size);
        info_output->color(FL_BACKGROUND_COLOR);

        info_group->resizable(info_output);
        info_group->end();

        // End
        Fl::lock();

        main_window->resizable(wizard);
        main_window->callback(quit);
        main_window->end();
        main_window->show(argc, argv);

        // Error check
        enum mpd_error mpderror;

        connection = mpd_connection_new(host, port, 0);

        mpderror = mpd_connection_get_error(connection);

        if (mpderror == MPD_ERROR_SUCCESS) {
            get_genres();

            chosen_genre_1->value(0);
            chosen_genre_2->value(0);
            chosen_genre_3->value(0);
            chosen_genre_3->do_callback();

            get_artists();

            artists_browser->value(1);
            artists_browser->do_callback();

            get_lists();

            lists_browser->value(1);
            lists_browser->do_callback();

            fill_queue();

            Fl::add_timeout(0.5, monitor);

        } else {
            const char *error_message = mpd_connection_get_error_message(connection);

            MPDC_Info_Window *infw = new MPDC_Info_Window(
                40 * 11,
                40 * 4,
                lc.warning
            );
            infw->button_close_label(lc.close);
            infw->textsize(font_size);
            infw->action_message(error_message);

            while (infw->visible()) {
                Fl::wait();
            }
        }

        if (music_directory == NULL) {
            display_images = false;

            MPDC_Info_Window *infw = new MPDC_Info_Window(
                40 * 11,
                40 * 4,
                lc.warning
            );
            infw->button_close_label(lc.close);
            infw->textsize(font_size);
            infw->action_message(lc.music_dir_not_set);

            while (infw->visible()) {
                Fl::wait();
            }
        }

    // Normal view
    } else {
        // Window
        main_window = new Fl_Double_Window(
            (40 * 4) * 5,
            40 * 26,
            APP_NAME
        );
        main_window->icon(px_mpdclient);
        main_window->begin();

        // Toggle buttons
        Fl_Group *group_toggle_buttons = new Fl_Group(
            main_window->x(),
            main_window->y(),
            main_window->w(),
            40 - 4,
            NULL
        );

        Fl_Box *toggle_buttons_box = new Fl_Box(
            FL_NO_BOX,
            group_toggle_buttons->x(),
            group_toggle_buttons->y(),
            group_toggle_buttons->w(),
            group_toggle_buttons->h(),
            NULL
        );

        // Toggle artists view
        Fl_Group *group_artists_button = new Fl_Group(
            toggle_buttons_box->x(),
            toggle_buttons_box->y(),
            toggle_buttons_box->w() / 4,
            toggle_buttons_box->h(),
            NULL
        );

        bt_artists = new Fl_Toggle_Button(
            group_artists_button->x() + 4,
            group_artists_button->y() + 4,
            group_artists_button->w() - 4,
            group_artists_button->h() - 4,
            lc.artists
        );
        bt_artists->labelsize(font_size);
        bt_artists->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_artists->callback(switch_view, (void *)1);
        bt_artists->value(1);

        group_artists_button->resizable(bt_artists);
        group_artists_button->end();

        // Toggle genres view
        Fl_Group *group_genres_button = new Fl_Group(
            group_artists_button->x() + group_artists_button->w(),
            toggle_buttons_box->y(),
            toggle_buttons_box->w() / 4,
            toggle_buttons_box->h(),
            NULL
        );

        bt_genres = new Fl_Toggle_Button(
            group_genres_button->x() + 4,
            group_genres_button->y() + 4,
            group_genres_button->w() - 4,
            group_genres_button->h() - 4,
            lc.genres
        );
        bt_genres->labelsize(font_size);
        bt_genres->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_genres->callback(switch_view, (void *)2);

        group_genres_button->resizable(bt_genres);
        group_genres_button->end();

        // Toggle lists view
        Fl_Group *group_lists_button = new Fl_Group(
            group_genres_button->x() + group_genres_button->w(),
            toggle_buttons_box->y(),
            toggle_buttons_box->w() / 4,
            toggle_buttons_box->h(),
            NULL
        );

        bt_lists = new Fl_Toggle_Button(
            group_lists_button->x() + 4,
            group_lists_button->y() + 4,
            group_lists_button->w() - 4,
            group_lists_button->h() - 4,
            lc.lists
        );
        bt_lists->labelsize(font_size);
        bt_lists->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_lists->callback(switch_view, (void *)3);

        group_lists_button->resizable(bt_lists);
        group_lists_button->end();

        // Toggle lyrics view
        Fl_Group *group_lyrics_button = new Fl_Group(
            group_lists_button->x() + group_lists_button->w(),
            toggle_buttons_box->y(),
            toggle_buttons_box->w() - group_artists_button->w() - group_genres_button->w() - group_lists_button->w(),
            toggle_buttons_box->h(),
            NULL
        );

        bt_lyrics = new Fl_Toggle_Button(
            group_lyrics_button->x() + 4,
            group_lyrics_button->y() + 4,
            group_lyrics_button->w() - 8,
            group_lyrics_button->h() - 4,
            lc.lyrics
        );
        bt_lyrics->labelsize(font_size);
        bt_lyrics->down_box(FL_FLAT_BOX);
        bt_lyrics->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        bt_lyrics->callback(switch_view, (void *)4);

        group_lyrics_button->resizable(bt_lyrics);
        group_lyrics_button->end();

        group_toggle_buttons->end();

        // Main resizable box
        Fl_Box *box = new Fl_Box(
            FL_NO_BOX,
            main_window->x(),
            main_window->y() + group_toggle_buttons->h(),
            main_window->w(),
            main_window->h() - 40 * 2 - group_toggle_buttons->h(),
            NULL
        );

        // Wizard
        wizard = new Fl_Wizard(
            box->x(),
            box->y(),
            box->w(),
            box->h() / 2
        );
        wizard->box(FL_NO_BOX);

        // Artists
        artists_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *artists_left_pane = new Fl_Group(
            artists_tab->x(),
            artists_tab->y(),
            artists_tab->w() / 3,
            artists_tab->h(),
            NULL
        );

        Fl_Box *artists_left_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            artists_left_pane->x(),
            artists_left_pane->y(),
            artists_left_pane->w(),
            artists_left_pane->h(),
            NULL
        );

        Fl_Box *artists_left_pane_box_inner = new Fl_Box(
            FL_DOWN_BOX,
            artists_left_pane_box_outer->x() + 4,
            artists_left_pane_box_outer->y() + 4,
            artists_left_pane_box_outer->w() - 4,
            artists_left_pane_box_outer->h() - 4,
            NULL
        );

        artists_browser = new Fl_Browser(
            artists_left_pane_box_inner->x() + 2,
            artists_left_pane_box_inner->y() + 2,
            artists_left_pane_box_inner->w() - 4,
            artists_left_pane_box_inner->h() - 44,
            NULL
        );
        artists_browser->textsize(font_size);
        artists_browser->color(FL_BACKGROUND2_COLOR);
        artists_browser->box(FL_FLAT_BOX);
        artists_browser->type(FL_HOLD_BROWSER);
        artists_browser->callback(get_albums);

        // Artists management
        Fl_Group *group_artists_management = new Fl_Group(
            artists_browser->x(),
            artists_browser->y() + artists_browser->h(),
            artists_browser->w(),
            40,
            NULL
        );

        Fl_Box *artists_left_pane_control_box = new Fl_Box(
            FL_UP_BOX,
            group_artists_management->x(),
            group_artists_management->y(),
            group_artists_management->w() - 40,
            group_artists_management->h(),
            NULL
        );

        button_search = new Fl_Button(
            artists_left_pane_control_box->x() + 4,
            artists_left_pane_control_box->y() + 4,
            32,
            artists_left_pane_control_box->h() - 8,
            NULL
        );
        button_search->box(FL_NO_BOX);
        button_search->down_box(FL_DOWN_BOX);
        button_search->image(px_search);
        button_search->deimage(px_search_inac);
        button_search->tooltip(lc.search);
        button_search->callback(search);
        button_search->deactivate();

        search_input = new Fl_Input(
            button_search->x() + button_search->w() + 4,
            artists_left_pane_control_box->y() + 4,
            artists_left_pane_control_box->w() - 32 - 4 * 3,
            artists_left_pane_control_box->h() - 8,
            NULL
        );
        search_input->textsize(font_size);
        search_input->color(FL_BACKGROUND2_COLOR);
        search_input->when(FL_WHEN_CHANGED);
        search_input->callback(search_input_check);

        // Update button
        Fl_Box *button_update_box = new Fl_Box(
            FL_UP_BOX,
            artists_left_pane_control_box->x() + artists_left_pane_control_box->w(),
            artists_left_pane_control_box->y(),
            group_artists_management->w() - artists_left_pane_control_box->w(),
            artists_left_pane_control_box->h(),
            NULL
        );

        button_update = new Fl_Button(
            button_update_box->x() + 4,
            button_update_box->y() + 4,
            button_update_box->w() - 8,
            button_update_box->h() - 8,
            NULL
        );
        button_update->box(FL_NO_BOX);
        button_update->down_box(FL_DOWN_BOX);
        button_update->image(px_update);
        button_update->deimage(px_update_inac);
        button_update->tooltip(lc.update);
        button_update->callback(update);

        group_artists_management->resizable(search_input);
        group_artists_management->end();

        artists_left_pane->resizable(artists_browser);
        artists_left_pane->end();

        // Albums by artist
        Fl_Group *artists_middle_pane = new Fl_Group(
            artists_left_pane->x() + artists_left_pane->w(),
            artists_left_pane->y(),
            artists_tab->w() / 3,
            artists_left_pane->h(),
            NULL
        );

        Fl_Box *artists_middle_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            artists_middle_pane->x(),
            artists_middle_pane->y(),
            artists_middle_pane->w(),
            artists_middle_pane->h(),
            NULL
        );

        int albums_browser_widths[] = {50, 350};

        albums_browser = new Fl_Browser(
            artists_middle_pane_box_outer->x() + 4,
            artists_middle_pane_box_outer->y() + 4,
            artists_middle_pane_box_outer->w() - 4,
            artists_middle_pane_box_outer->h() - 4,
            NULL
        );
        albums_browser->textsize(font_size);
        albums_browser->color(FL_BACKGROUND2_COLOR);
        albums_browser->type(FL_HOLD_BROWSER);
        albums_browser->column_widths(albums_browser_widths);
        albums_browser->column_char('\t');
        albums_browser->callback(get_songs);

        artists_middle_pane->resizable(albums_browser);
        artists_middle_pane->end();

        // Songs by album
        Fl_Group *artists_right_pane = new Fl_Group(
            artists_middle_pane->x() + artists_middle_pane->w(),
            artists_middle_pane->y(),
            artists_tab->w() - artists_left_pane->w() - artists_middle_pane->w(),
            artists_middle_pane->h(),
            NULL
        );

        Fl_Box *artists_right_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            artists_right_pane->x(),
            artists_right_pane->y(),
            artists_right_pane->w(),
            artists_right_pane->h(),
            NULL
        );

        int songs_browser_widths[] = {30, 350};

        songs_browser = new Fl_Browser(
            artists_right_pane_box_outer->x() + 4,
            artists_right_pane_box_outer->y() + 4,
            artists_right_pane_box_outer->w() - 8,
            artists_right_pane_box_outer->h() - 4,
            NULL
        );
        songs_browser->textsize(font_size);
        songs_browser->color(FL_BACKGROUND2_COLOR);
        songs_browser->type(FL_HOLD_BROWSER);
        songs_browser->column_widths(songs_browser_widths);
        songs_browser->column_char('\t');
        songs_browser->callback(add_song);

        artists_right_pane->resizable(songs_browser);
        artists_right_pane->end();

        artists_tab->end();

        // Genres
        genres_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *genres_left_pane = new Fl_Group(
            genres_tab->x(),
            genres_tab->y(),
            genres_tab->w() / 2,
            genres_tab->h(),
            NULL
        );

        Fl_Box *genres_left_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            genres_left_pane->x(),
            genres_left_pane->y(),
            genres_left_pane->w(),
            genres_left_pane->h(),
            NULL
        );

        Fl_Box *genres_left_pane_box_inner = new Fl_Box(
            FL_DOWN_BOX,
            genres_left_pane_box_outer->x() + 4,
            genres_left_pane_box_outer->y() + 4,
            genres_left_pane_box_outer->w() - 4,
            genres_left_pane_box_outer->h() - 4,
            NULL
        );

        int genres_albums_browser_widths[] = {50, 350};

        genres_albums_browser = new Fl_Browser(
            genres_left_pane_box_inner->x() + 2,
            genres_left_pane_box_inner->y() + 2,
            genres_left_pane_box_inner->w() - 4,
            genres_left_pane_box_inner->h() - 44,
            NULL
        );
        genres_albums_browser->textsize(font_size);
        genres_albums_browser->color(FL_BACKGROUND2_COLOR);
        genres_albums_browser->box(FL_FLAT_BOX);
        genres_albums_browser->type(FL_HOLD_BROWSER);
        genres_albums_browser->column_widths(genres_albums_browser_widths);
        genres_albums_browser->column_char('\t');
        genres_albums_browser->callback(get_genres_songs);

        // Genres management
        Fl_Group *group_genres_management = new Fl_Group(
            genres_albums_browser->x(),
            genres_albums_browser->y() + genres_albums_browser->h(),
            genres_albums_browser->w(),
            40,
            NULL
        );

        Fl_Box *genres_control_box = new Fl_Box(
            FL_UP_BOX,
            group_genres_management->x(),
            group_genres_management->y(),
            group_genres_management->w(),
            group_genres_management->h(),
            NULL
        );

        // One
        Fl_Group *group_genre_1 = new Fl_Group(
            genres_control_box->x(),
            genres_control_box->y(),
            genres_control_box->w() / 3,
            genres_control_box->h(),
            NULL
        );

        Fl_Box *genre_box_1 = new Fl_Box(
            FL_NO_BOX,
            group_genre_1->x(),
            group_genre_1->y(),
            group_genre_1->w(),
            group_genre_1->h(),
            NULL
        );

        chosen_genre_1 = new MPDC_Choice(
            genre_box_1->x() + 4,
            genre_box_1->y() + 4,
            genre_box_1->w() - 4,
            genre_box_1->h() - 8,
            NULL
        );
        chosen_genre_1->textsize(font_size);
        chosen_genre_1->callback(get_genres_albums);

        group_genre_1->resizable(chosen_genre_1);
        group_genre_1->end();

        // Two
        Fl_Group *group_genre_2 = new Fl_Group(
            group_genre_1->x() + group_genre_1->w(),
            genres_control_box->y(),
            genres_control_box->w() / 3,
            genres_control_box->h(),
            NULL
        );

        Fl_Box *genre_box_2 = new Fl_Box(
            FL_NO_BOX,
            group_genre_2->x(),
            group_genre_2->y(),
            group_genre_2->w(),
            group_genre_2->h(),
            NULL
        );

        chosen_genre_2 = new MPDC_Choice(
            genre_box_2->x() + 4,
            genre_box_2->y() + 4,
            genre_box_2->w() - 4,
            genre_box_2->h() - 8,
            NULL
        );
        chosen_genre_2->textsize(font_size);
        chosen_genre_2->callback(get_genres_albums);

        group_genre_2->resizable(chosen_genre_2);
        group_genre_2->end();

        // Three
        Fl_Group *group_genre_3 = new Fl_Group(
            group_genre_2->x() + group_genre_2->w(),
            genres_control_box->y(),
            genres_control_box->w() - group_genre_1->w() - group_genre_2->w(),
            genres_control_box->h(),
            NULL
        );

        Fl_Box *genre_box_3 = new Fl_Box(
            FL_NO_BOX,
            group_genre_3->x(),
            group_genre_3->y(),
            group_genre_3->w(),
            group_genre_3->h(),
            NULL
        );

        chosen_genre_3 = new MPDC_Choice(
            genre_box_3->x() + 4,
            genre_box_3->y() + 4,
            genre_box_3->w() - 8,
            genre_box_3->h() - 8,
            NULL
        );
        chosen_genre_3->textsize(font_size);
        chosen_genre_3->callback(get_genres_albums);

        group_genre_3->resizable(chosen_genre_3);
        group_genre_3->end();

        group_genres_management->end();

        genres_left_pane->resizable(genres_albums_browser);
        genres_left_pane->end();

        // Songs by genre
        Fl_Group *genres_right_pane = new Fl_Group(
            genres_left_pane->x() + genres_left_pane->w(),
            genres_tab->y(),
            genres_tab->w() - genres_left_pane->w(),
            genres_tab->h(),
            NULL
        );

        Fl_Box *genres_right_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            genres_right_pane->x(),
            genres_right_pane->y(),
            genres_right_pane->w(),
            genres_right_pane->h(),
            NULL
        );

        genres_songs_browser = new Fl_Browser(
            genres_right_pane_box_outer->x() + 4,
            genres_right_pane_box_outer->y() + 4,
            genres_right_pane_box_outer->w() - 8,
            genres_right_pane_box_outer->h() - 4,
            NULL
        );
        genres_songs_browser->textsize(font_size);
        genres_songs_browser->color(FL_BACKGROUND2_COLOR);
        genres_songs_browser->type(FL_HOLD_BROWSER);
        genres_songs_browser->column_widths(songs_browser_widths);
        genres_songs_browser->column_char('\t');
        genres_songs_browser->callback(add_genre_song);

        genres_right_pane->resizable(genres_songs_browser);
        genres_right_pane->end();

        genres_tab->end();

        // Lists
        lists_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *lists_left_pane = new Fl_Group(
            lists_tab->x(),
            lists_tab->y(),
            lists_tab->w() / 2,
            lists_tab->h(),
            NULL
        );

        Fl_Box *lists_left_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            lists_left_pane->x(),
            lists_left_pane->y(),
            lists_left_pane->w(),
            lists_left_pane->h(),
            NULL
        );

        Fl_Box *lists_left_pane_box_inner = new Fl_Box(
            FL_DOWN_BOX,
            lists_left_pane_box_outer->x() + 4,
            lists_left_pane_box_outer->y() + 4,
            lists_left_pane_box_outer->w() - 4,
            lists_left_pane_box_outer->h() - 4,
            NULL
        );
        lists_left_pane_box_inner->color(FL_BACKGROUND2_COLOR);

        lists_browser = new Fl_Browser(
            lists_left_pane_box_inner->x() + 2,
            lists_left_pane_box_inner->y() + 2,
            lists_left_pane_box_inner->w() - 4,
            lists_left_pane_box_inner->h() - 44,
            NULL
        );
        lists_browser->textsize(font_size);
        lists_browser->color(FL_BACKGROUND2_COLOR);
        lists_browser->box(FL_FLAT_BOX);
        lists_browser->type(FL_HOLD_BROWSER);
        lists_browser->callback(get_list_songs);

        // List management
        Fl_Group *group_list_management = new Fl_Group(
            lists_browser->x(),
            lists_browser->y() + lists_browser->h(),
            lists_browser->w(),
            40,
            NULL
        );

        Fl_Box *lists_control_box = new Fl_Box(
            FL_UP_BOX,
            group_list_management->x(),
            group_list_management->y(),
            group_list_management->w(),
            group_list_management->h(),
            NULL
        );

        button_rename = new Fl_Button(
            lists_control_box->x() + 4,
            lists_control_box->y() + 4,
            32,
            lists_control_box->h() - 8,
            NULL
        );
        button_rename->box(FL_NO_BOX);
        button_rename->down_box(FL_DOWN_BOX);
        button_rename->image(px_rename);
        button_rename->deimage(px_rename_inac);
        button_rename->tooltip(lc.ren);
        button_rename->callback(rename_list);
        button_rename->deactivate();

        Fl_Box *lists_hidden_box = new Fl_Box(
            FL_NO_BOX,
            button_rename->x() + button_rename->w() + 4,
            lists_control_box->y() + 4,
            lists_control_box->w() - 32 * 2 - 4 * 4,
            lists_control_box->h() - 8,
            NULL
        );

        button_delete = new Fl_Button(
            lists_hidden_box->x() + lists_hidden_box->w() + 4,
            lists_control_box->y() + 4,
            32,
            lists_control_box->h() - 8,
            NULL
        );
        button_delete->box(FL_NO_BOX);
        button_delete->down_box(FL_DOWN_BOX);
        button_delete->image(px_delete);
        button_delete->deimage(px_delete_inac);
        button_delete->tooltip(lc.del);
        button_delete->callback(delete_list);
        button_delete->deactivate();

        group_list_management->resizable(lists_hidden_box);
        group_list_management->end();

        lists_left_pane->resizable(lists_browser);
        lists_left_pane->end();

        // Songs by list
        Fl_Group *lists_right_pane = new Fl_Group(
            lists_tab->x() + lists_left_pane->w(),
            lists_tab->y(),
            lists_tab->w() - lists_left_pane->w(),
            lists_tab->h(),
            NULL
        );

        Fl_Box *lists_right_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            lists_right_pane->x(),
            lists_right_pane->y(),
            lists_right_pane->w(),
            lists_right_pane->h(),
            NULL
        );

        list_songs_browser = new Fl_Browser(
            lists_right_pane_box_outer->x() + 4,
            lists_right_pane_box_outer->y() + 4,
            lists_right_pane_box_outer->w() - 8,
            lists_right_pane_box_outer->h() - 4,
            NULL
        );
        list_songs_browser->textsize(font_size);
        list_songs_browser->color(FL_BACKGROUND2_COLOR);
        list_songs_browser->type(FL_HOLD_BROWSER);
        list_songs_browser->callback(add_list_song);

        lists_right_pane->resizable(list_songs_browser);
        lists_right_pane->end();

        lists_tab->end();

        // Lyrics
        lyrics_tab = new Fl_Group(
            wizard->x(),
            wizard->y(),
            wizard->w(),
            wizard->h(),
            NULL
        );

        Fl_Group *lyrics_pane = new Fl_Group(
            lyrics_tab->x(),
            lyrics_tab->y(),
            lyrics_tab->w(),
            lyrics_tab->h(),
            NULL
        );

        Fl_Box *lyrics_pane_box_outer = new Fl_Box(
            FL_NO_BOX,
            lyrics_pane->x(),
            lyrics_pane->y(),
            lyrics_pane->w(),
            lyrics_pane->h(),
            NULL
        );

        lyrics_buffer = new Fl_Text_Buffer();
        lyrics_buffer->add_modify_callback(save_lyrics, NULL);

        lyrics_viewer = new Fl_Text_Editor(
            lyrics_pane_box_outer->x() + 4,
            lyrics_pane_box_outer->y() + 4,
            lyrics_pane_box_outer->w() - 8,
            lyrics_pane_box_outer->h() - 4,
            NULL
        );
        lyrics_viewer->textsize(font_size);
        lyrics_viewer->color(FL_BACKGROUND2_COLOR);
        lyrics_viewer->wrap_mode(Fl_Text_Display::WRAP_AT_BOUNDS, 0);

        lyrics_pane->resizable(lyrics_viewer);
        lyrics_pane->end();

        lyrics_tab->end();

        wizard->end();

        // Images
        Fl_Group *group_images;

        if (display_images) {
            group_images = new Fl_Group(
                box->x(),
                box->y() + wizard->h(),
                box->w() / 3,
                box->h() - wizard->h(),
                NULL
            );

            Fl_Box *images_box_outer = new Fl_Box(
                FL_NO_BOX,
                group_images->x(),
                group_images->y(),
                group_images->w(),
                group_images->h(),
                NULL
            );

            Fl_Box *images_box_inner = new Fl_Box(
                FL_DOWN_BOX,
                images_box_outer->x() + 4,
                images_box_outer->y() + 4,
                images_box_outer->w() - 4,
                images_box_outer->h() - 4,
                NULL
            );

            images_box = new Fl_Box(
                FL_UP_BOX,
                images_box_inner->x() + 2,
                images_box_inner->y() + 2,
                images_box_inner->w() - 4,
                images_box_inner->h() - 4,
                NULL
            );

            logo_box = new Fl_Box(
                FL_BORDER_BOX,
                images_box->x(),
                images_box->y(),
                images_box->w(),
                images_box->h() / 3,
                NULL
            );
            logo_box->hide();

            artist_box = new Fl_Box(
                FL_BORDER_BOX,
                images_box->x(),
                images_box->y() + logo_box->h(),
                images_box->w(),
                images_box->h() / 3,
                NULL
            );
            artist_box->hide();

            cover_box = new Fl_Box(
                FL_BORDER_BOX,
                images_box->x(),
                images_box->y() + logo_box->h() + artist_box->h(),
                images_box->w(),
                images_box->h() - logo_box->h() - artist_box->h(),
                NULL
            );
            cover_box->hide();

            group_images->resizable(images_box);
            group_images->end();
        }

        // Queue
        Fl_Group *group_queue;

        if (display_images) {
            group_queue = new Fl_Group(
                group_images->x() + group_images->w(),
                group_images->y(),
                box->w() - group_images->w(),
                group_images->h(),
                NULL
            );

        } else {
            group_queue = new Fl_Group(
                box->x(),
                box->y() + wizard->h(),
                box->w(),
                box->h() - wizard->h(),
                NULL
            );
        }

        Fl_Box *queue_box_outer = new Fl_Box(
            FL_NO_BOX,
            group_queue->x(),
            group_queue->y(),
            group_queue->w(),
            group_queue->h(),
            NULL
        );

        int widths[] = {30, 265, 75, 85, 200, 200, 0};

        const char *headers[6] = {lc.track, lc.title, lc.duration, lc.year, lc.artist, lc.album};

        queue_browser = new MPDC_Browser(
            queue_box_outer->x() + 4,
            queue_box_outer->y() + 4,
            queue_box_outer->w() - 8,
            queue_box_outer->h() - 4,
            NULL
        );
        queue_browser->text_size(font_size);
        queue_browser->column_widths(widths);
        queue_browser->column_char('\t');
        queue_browser->total_columns(6);
        queue_browser->column_headers(headers);
        queue_browser->browser->callback(queue_cb);

        group_queue->resizable(queue_browser);
        group_queue->end();

        // Playback control stuff
        Fl_Group *group_playback_control = new Fl_Group(
            box->x(),
            box->y() + box->h(),
            box->w(),
            40,
            NULL
        );

        Fl_Button *bt_bw = new Fl_Button(
            group_playback_control->x() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_bw->box(FL_FLAT_BOX);
        bt_bw->down_box(FL_DOWN_BOX);
        bt_bw->image(px_backward);
        bt_bw->tooltip(lc.prev);
        bt_bw->callback(play_prev);

        Fl_Button *bt_fw = new Fl_Button(
            bt_bw->x() + bt_bw->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_fw->box(FL_FLAT_BOX);
        bt_fw->down_box(FL_DOWN_BOX);
        bt_fw->image(px_forward);
        bt_fw->tooltip(lc.next);
        bt_fw->callback(play_next);

        Fl_Button *bt_pl = new Fl_Button(
            bt_fw->x() + bt_fw->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_pl->box(FL_FLAT_BOX);
        bt_pl->down_box(FL_DOWN_BOX);
        bt_pl->image(px_play);
        bt_pl->tooltip(lc.play);
        bt_pl->callback(play);

        button_pause = new Fl_Button(
            bt_pl->x() + bt_pl->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_pause->box(FL_FLAT_BOX);
        button_pause->down_box(FL_DOWN_BOX);
        button_pause->image(px_pause);
        button_pause->tooltip(lc.pause);
        button_pause->callback(toggle_pause);

        Fl_Button *bt_st = new Fl_Button(
            button_pause->x() + button_pause->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_st->box(FL_FLAT_BOX);
        bt_st->down_box(FL_DOWN_BOX);
        bt_st->image(px_stop);
        bt_st->tooltip(lc.stop);
        bt_st->callback(stop);

        Fl_Button *bt_sh = new Fl_Button(
            bt_st->x() + bt_st->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_sh->box(FL_FLAT_BOX);
        bt_sh->down_box(FL_DOWN_BOX);
        bt_sh->image(px_shuffle);
        bt_sh->tooltip(lc.shuffle);
        bt_sh->callback(shuffle);

        button_random = new Fl_Button(
            bt_sh->x() + bt_sh->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_random->box(FL_FLAT_BOX);
        button_random->down_box(FL_DOWN_BOX);
        button_random->image(px_random);
        button_random->tooltip(lc.random);
        button_random->callback(random);

        button_repeat = new Fl_Button(
            button_random->x() + button_random->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_repeat->box(FL_FLAT_BOX);
        button_repeat->down_box(FL_DOWN_BOX);
        button_repeat->image(px_repeat);
        button_repeat->tooltip(lc.repeat);
        button_repeat->callback(repeat);

        button_crossfade = new Fl_Button(
            button_repeat->x() + button_repeat->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_crossfade->box(FL_FLAT_BOX);
        button_crossfade->down_box(FL_DOWN_BOX);
        button_crossfade->image(px_crossfade);
        button_crossfade->tooltip(lc.crossfade);
        button_crossfade->callback(crossfade);

        input_crossfade = new Fl_Value_Input(
            button_crossfade->x() + button_crossfade->w() + 4,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        input_crossfade->textsize(font_size);
        input_crossfade->color(FL_BACKGROUND2_COLOR);
        input_crossfade->tooltip(lc.crossfade);
        input_crossfade->soft();
        input_crossfade->minimum(0);
        input_crossfade->value(crossfade_sec);
        input_crossfade->callback(set_crossfade);

        Fl_Button *bt_cl = new Fl_Button(
            group_playback_control->w() - 4 - 32,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        bt_cl->box(FL_FLAT_BOX);
        bt_cl->down_box(FL_DOWN_BOX);
        bt_cl->image(px_clear);
        bt_cl->tooltip(lc.clear);
        bt_cl->callback(clear_queue);

        button_save = new Fl_Button(
            bt_cl->x() - 4 - 32,
            group_playback_control->y() + 4,
            32,
            32,
            NULL
        );
        button_save->box(FL_FLAT_BOX);
        button_save->down_box(FL_DOWN_BOX);
        button_save->image(px_save);
        button_save->deimage(px_save_inac);
        button_save->tooltip(lc.save);
        button_save->callback(save_list);

        // Progress bar
        prog_bar = new MPDC_Prog_Bar(
            input_crossfade->x() + input_crossfade->w() + 4,
            group_playback_control->y() + 4,
            group_playback_control->w() - 32 * 12 - 4 * 14,
            group_playback_control->h() - 8,
            NULL
        );
        prog_bar->minimum(0);
        prog_bar->maximum(1);
        prog_bar->callback(rewind);
        prog_bar->timetip->textsize(font_size);

        group_playback_control->resizable(prog_bar);
        group_playback_control->end();

        // Info bar
        Fl_Group *info_group = new Fl_Group(
            group_playback_control->x(),
            group_playback_control->y() + group_playback_control->h(),
            group_playback_control->w(),
            40
        );

        info_output = new Fl_Output(
            info_group->x() + 4,
            info_group->y() + 4,
            info_group->w() - 8,
            info_group->h() - 8,
            NULL
        );
        info_output->textsize(font_size);
        info_output->color(FL_BACKGROUND_COLOR);

        info_group->resizable(info_output);
        info_group->end();

        // End
        Fl::lock();

        main_window->resizable(box);
        main_window->callback(quit);
        main_window->end();
        main_window->show(argc, argv);

        // Error check
        enum mpd_error mpderror;

        connection = mpd_connection_new(host, port, 0);

        mpderror = mpd_connection_get_error(connection);

        if (mpderror == MPD_ERROR_SUCCESS) {
            get_genres();

            chosen_genre_1->value(0);
            chosen_genre_2->value(0);
            chosen_genre_3->value(0);
            chosen_genre_3->do_callback();

            get_artists();

            artists_browser->value(1);
            artists_browser->do_callback();

            get_lists();

            lists_browser->value(1);
            lists_browser->do_callback();

            fill_queue();

            Fl::add_timeout(0.5, monitor);

        } else {
            const char *error_message = mpd_connection_get_error_message(connection);

            MPDC_Info_Window *infw = new MPDC_Info_Window(
                40 * 11,
                40 * 4,
                lc.warning
            );
            infw->button_close_label(lc.close);
            infw->textsize(font_size);
            infw->action_message(error_message);

            while (infw->visible()) {
                Fl::wait();
            }
        }

        if (music_directory == NULL) {
            main_window->remove(group_images);

            delete group_images;

            group_queue->resize(
                box->x(),
                box->y() + wizard->h(),
                box->w(),
                box->h() - wizard->h()
            );
            group_queue->redraw();

            display_images = false;

            MPDC_Info_Window *infw = new MPDC_Info_Window(
                40 * 11,
                40 * 4,
                lc.warning
            );
            infw->button_close_label(lc.close);
            infw->textsize(font_size);
            infw->action_message(lc.music_dir_not_set);

            while (infw->visible()) {
                Fl::wait();
            }
        }
    }

    return Fl::run();
}

// SWITCH VIEW
void switch_view(Fl_Widget *widget, void *data) {
    Fl_Toggle_Button *bt = (Fl_Toggle_Button *)widget;

    long i = (long)data;

    if (compact) {
        bt_cover_art->value(0);
        bt_artists->value(0);
        bt_genres->value(0);
        bt_lists->value(0);
        bt_lyrics->value(0);
        bt_queue->value(0);

        bt->value(1);

        if (i == 1)
            wizard->value(artwork_tab);

        if (i == 2) {
            wizard->value(artists_tab);

            if (albums_browser->value() != 0)
                albums_browser->do_callback();
        }

        if (i == 3) {
            wizard->value(genres_tab);

            if (genres_albums_browser->value() != 0)
                genres_albums_browser->do_callback();
        }

        if (i == 4)
            wizard->value(lists_tab);

        if (i == 5)
            wizard->value(lyrics_tab);

        if (i == 6)
            wizard->value(queue_tab);

    // Normal view
    } else {
        bt_artists->value(0);
        bt_genres->value(0);
        bt_lists->value(0);
        bt_lyrics->value(0);

        bt->value(1);

        if (i == 1) {
            wizard->value(artists_tab);

            if (albums_browser->value() != 0)
                albums_browser->do_callback();
        }

        if (i == 2) {
            wizard->value(genres_tab);

            if (genres_albums_browser->value() != 0)
                genres_albums_browser->do_callback();
        }

        if (i == 3)
            wizard->value(lists_tab);

        if (i == 4)
            wizard->value(lyrics_tab);
    }
}

// MONITOR PROCESS
void monitor(void *) {
    struct mpd_status *status = NULL;

    static enum mpd_state prev_state = MPD_STATE_UNKNOWN;

    static enum mpd_error mpdpreverror = MPD_ERROR_SUCCESS;

    enum mpd_state state = MPD_STATE_UNKNOWN;

    enum mpd_error mpderror;

    static int prev_song_pos = -1;
    static int current_song_pos = 0;

    static unsigned queue_version = 0;
    static unsigned queue_duration = 0;
    static unsigned queue_length = 0;

    unsigned queue_version_new = 0;

    char info_string[255] = "";

    if (connection)
        mpd_connection_free(connection);

    connection = mpd_connection_new(host, port, 0);

    // Error check
    mpderror = mpd_connection_get_error(connection);

    if (mpderror != MPD_ERROR_SUCCESS) {
        if (mpdpreverror != mpderror) {
            mpdpreverror = mpderror;

            const char *error_message = mpd_connection_get_error_message(connection);

            MPDC_Info_Window *infw = new MPDC_Info_Window(
                40 * 11,
                40 * 4,
                lc.warning
            );
            infw->button_close_label(lc.close);
            infw->textsize(font_size);
            infw->action_message(error_message);

            while (infw->visible()) {
                Fl::wait();
            }
        }

        return;
    }

    status = mpd_status_begin();
    status = mpd_run_status(connection);

    state = mpd_status_get_state(status);

    // Update queue
    queue_version_new = mpd_status_get_queue_version(status);

    if (queue_version != queue_version_new) {
        queue_version = queue_version_new;

        queue_duration = fill_queue();

        // Save button status
        queue_length = mpd_status_get_queue_length(status);

        if (queue_length > 0)
            button_save->activate();
        else
            button_save->deactivate();

        button_save->redraw();
    }

    // Random button status
    if (mpd_status_get_random(status) > 0)
        button_random->image(px_random_on);
    else
        button_random->image(px_random);

    button_random->redraw();

    // Repeat button status
    if (mpd_status_get_repeat(status) > 0)
        button_repeat->image(px_repeat_on);
    else
        button_repeat->image(px_repeat);

    button_repeat->redraw();

    // Crossfade button status
    if (mpd_status_get_crossfade(status) > 0)
        button_crossfade->image(px_crossfade_on);
    else
        button_crossfade->image(px_crossfade);

    button_crossfade->redraw();

    if (state == MPD_STATE_PLAY || state == MPD_STATE_PAUSE) {
        const struct mpd_audio_format *quality = NULL;

        struct mpd_song *current_song = NULL;

        unsigned time = 0;
        unsigned elapsed_time = 0;
        unsigned total_time = 0;
        unsigned kbps = 0;
        unsigned bits = 0;
        unsigned sample_rate = 0;
        unsigned channels = 0;

        // Set old state
        prev_state = state;

        // Pause button status
        if (state == MPD_STATE_PAUSE)
            button_pause->image(px_paused);
        else
            button_pause->image(px_pause);

        button_pause->redraw();

        // Current song status
        current_song = mpd_run_current_song(connection);

        const char *song_uri = mpd_song_get_uri(current_song);
        const char *song_title = mpd_song_get_tag(current_song, MPD_TAG_TITLE, 0);

        main_window->label(song_title ? song_title : APP_NAME);

        current_song_pos = mpd_status_get_song_pos(status);

        if (prev_song_pos != current_song_pos) {
            prev_song_pos = current_song_pos;

            lyrics_viewer->buffer(NULL);

            if (!is_stream(song_uri) && music_directory) {
                if (display_images)
                    get_images(song_uri);

                show_lyrics(song_uri);

                lyrics_viewer->buffer(lyrics_buffer);
            }

            lyrics_viewer->redraw();
            lyrics_viewer->hide();
            lyrics_viewer->show();
        }

        // Selection status
        if (!user_grabbed_sel)
            queue_browser->select(current_song_pos + 1);

        // Time and progress
        total_time = mpd_status_get_total_time(status);

        elapsed_time = mpd_status_get_elapsed_time(status);

        if (total_time >= elapsed_time) {
            float progress = ((float)elapsed_time / (float)total_time);

            prog_bar->value(progress);
            prog_bar->get_song_time(total_time);

            time = get_time_left(queue_duration, current_song_pos, elapsed_time);

        } else {
            prog_bar->value(1);
            prog_bar->get_song_time(0);

            time = elapsed_time;
        }

        // Quality
        kbps = mpd_status_get_kbit_rate(status);

        quality = mpd_status_get_audio_format(status);

        if (quality) {
            bits = quality->bits;
            sample_rate = quality->sample_rate;
            channels = quality->channels;
        }

        // Show
        snprintf(
            info_string,
            255,
            "%d %s | %02d:%02d:%02d | %d %s | %d %s | %d %s | %s",
            queue_length,
            lc.items,
            time / 3600,
            time % 3600 / 60,
            time % 60,
            kbps,
            lc.kbps,
            bits,
            lc.bits,
            sample_rate,
            lc.sample_rate,
            channels == 1 ? lc.mono : lc.stereo
        );

        info_output->value(info_string);

        if (current_song)
            mpd_song_free(current_song);

    } else {
        // Selection status
        if (!user_grabbed_sel)
            queue_browser->deselect();

        // Reset stuff
        if (prev_state != state) {
            prev_state = state;

            prev_song_pos = -1;

            main_window->label(APP_NAME);

            button_pause->image(px_pause);
            button_pause->redraw();

            prog_bar->value(0);
            prog_bar->get_song_time(0);

            lyrics_viewer->buffer(NULL);
            lyrics_viewer->redraw();
            lyrics_viewer->hide();
            lyrics_viewer->show();
        }

        // Show
        const unsigned *server_version = mpd_connection_get_server_version(connection);

        snprintf(
            info_string,
            255,
            "%s %s | MPD %d.%d.%d | %d %s | %d %s %d %s %d %s",
            APP_NAME,
            APP_VERSION,
            server_version[0],
            server_version[1],
            server_version[2],
            queue_length,
            lc.items,
            queue_duration / 3600,
            lc.hours,
            queue_duration % 3600 / 60,
            lc.minutes,
            queue_duration % 60,
            lc.seconds
        );

        info_output->value(info_string);
    }

    if (status)
        mpd_status_free(status);

    Fl::repeat_timeout(0.5, monitor);
}

// GET TIME LEFT
unsigned get_time_left(unsigned queue_duration, int current_pos, unsigned elapsed_time) {
    struct mpd_connection *local = NULL;

    static unsigned prev_tracks_elapsed_time = 0;

    static int old_pos = -1;

    int counter = 0;

    struct mpd_entity *queue_entity = NULL;

    if (old_pos != current_pos) {
        old_pos = current_pos;

        prev_tracks_elapsed_time = 0;

        local = mpd_connection_new(host, port, 0);

        mpd_send_list_queue_meta(local);

        while ((queue_entity = mpd_recv_entity(local)) != NULL) {
            if (mpd_entity_get_type(queue_entity) == MPD_ENTITY_TYPE_SONG) {
                const struct mpd_song *song = mpd_entity_get_song(queue_entity);

                unsigned song_duration = mpd_song_get_duration(song);

                if (counter < current_pos)
                    prev_tracks_elapsed_time += song_duration;
                else
                    mpd_search_cancel(local);

                counter++;

                if (song)
                    mpd_song_free((struct mpd_song *)song);
            }
        }
    }

    if (local)
        mpd_connection_free(local);

    return (queue_duration - (prev_tracks_elapsed_time + elapsed_time));
}

// FILL QUEUE
unsigned fill_queue(void) {
    struct mpd_connection *local = NULL;

    char queue_row[200] = "";

    unsigned queue_duration = 0;

    struct mpd_entity *queue_entity = NULL;

    queue_browser->clear();

    local = mpd_connection_new(host, port, 0);

    mpd_send_list_queue_meta(local);

    while ((queue_entity = mpd_recv_entity(local)) != NULL) {
        if (mpd_entity_get_type(queue_entity) == MPD_ENTITY_TYPE_SONG) {
            const struct mpd_song *song = mpd_entity_get_song(queue_entity);

            unsigned song_duration = mpd_song_get_duration(song);

            const char *song_uri    = mpd_song_get_uri(song);
            const char *song_track  = mpd_song_get_tag(song, MPD_TAG_TRACK,  0);
            const char *song_title  = mpd_song_get_tag(song, MPD_TAG_TITLE,  0);
            const char *song_year   = mpd_song_get_tag(song, MPD_TAG_DATE,   0);
            const char *song_artist = mpd_song_get_tag(song, MPD_TAG_ARTIST, 0);
            const char *song_album  = mpd_song_get_tag(song, MPD_TAG_ALBUM,  0);
            const char *song_name   = mpd_song_get_tag(song, MPD_TAG_NAME,   0);
            const char *track       = NULL;

            char trackf[3] = "00";

            if (song_track) {
                if (strlen(song_track) == 1) {
                    trackf[1] = song_track[0];

                    track = trackf;

                } else {
                    track = song_track;
                }

            } else {
                track = lc.blank;
            }

            queue_duration += song_duration;

            if (song_duration > 0)
                snprintf(
                    queue_row,
                    200,
                    "%s\t%s\t%02d:%02d:%02d\t@r%s\t%s\t%s",
                    track,
                    song_title ? song_title : song_uri,
                    song_duration / 3600,
                    song_duration % 3600 / 60,
                    song_duration % 60,
                    song_year ? song_year : lc.blank,
                    song_artist ? song_artist : lc.blank,
                    song_album ? song_album : lc.blank
                );
            else
                snprintf(
                    queue_row,
                    200,
                    "%s\t%s\t%s\t@r%s\t%s\t%s",
                    track,
                    song_title ? song_title : song_uri,
                    lc.blank,
                    song_year ? song_year : lc.blank,
                    song_artist ? song_artist : lc.blank,
                    song_name ? song_name : lc.blank
                );

            queue_browser->add(queue_row);

            if (song)
                mpd_song_free((struct mpd_song *)song);
        }
    }

    if (queue_entity)
        mpd_entity_free(queue_entity);

    if (local)
        mpd_connection_free(local);

    return queue_duration;
}

// FILL ARTISTS BROWSER
void get_artists(void) {
    struct mpd_connection *local = NULL;

    struct mpd_pair *pair = NULL;

    artists_browser->clear();

    local = mpd_connection_new(host, port, 0);

    mpd_search_db_tags(local, MPD_TAG_ARTIST);

    mpd_search_commit(local);

    while ((pair = mpd_recv_pair_tag(local, MPD_TAG_ARTIST)) != NULL) {
        artists_browser->add(pair->value);

        mpd_return_pair(local, pair);
    }

    if (local)
        mpd_connection_free(local);
}

// FILL ALBUMS BROWSER
void get_albums(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    struct mpd_song *song = NULL;

    int artist_line = artists_browser->value();
    int album_line = 1;

    if (!artist_line)
        return;

    albums_browser->clear();

    local = mpd_connection_new(host, port, 0);

    mpd_search_db_songs(local, true);

    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_ARTIST, artists_browser->text(artist_line));

    mpd_search_add_sort_tag(local, MPD_TAG_DATE, false);

    mpd_search_commit(local);

    while ((song = mpd_recv_song(local)) != NULL) {
        const char *album = mpd_song_get_tag(song, MPD_TAG_ALBUM, 0);
        const char *year = mpd_song_get_tag(song, MPD_TAG_DATE, 0);

        const char *prev_album = albums_browser->text(album_line);

        char song_string[500] = "";

        if (prev_album == NULL) {
            snprintf(song_string, 500, "%s - %s", year, album);

            albums_browser->add(song_string);

        } else if (strcmp(prev_album + 7, album)) {
            snprintf(song_string, 500, "%s - %s", year, album);

            albums_browser->add(song_string);

            album_line++;
        }
    }

    if (song)
        mpd_song_free(song);

    albums_browser->value(1);

    // Add artist songs to queue
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        mpd_search_add_db_songs(local, true);

        mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_ARTIST, artists_browser->text(artist_line));

        mpd_search_commit(local);

    } else {
        albums_browser->do_callback();
    }

    if (local)
        mpd_connection_free(local);
}

// FILL SONGS BROWSER
void get_songs(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    char *year = NULL;
    char *album = NULL;

    struct mpd_song *song = NULL;

    int i;
    int album_line = albums_browser->value();

    if (!album_line)
        return;

    songs_browser->clear();

    const char *year_and_album = albums_browser->text(album_line);

    // Get year
    for (i = 0; ; i++) {
        if (year_and_album[i] == ' ' || year_and_album[i] == '\0')
            break;
    }

    year = strndup(year_and_album, i);

    // Get album
    album = strdup(year_and_album + i + 3);

    // Get songs
    local = mpd_connection_new(host, port, 0);

    mpd_search_db_songs(local, true);

    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_ALBUM, album);
    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_DATE, year);

    mpd_search_add_sort_tag(local, MPD_TAG_DATE, false);

    mpd_search_commit(local);

    while ((song = mpd_recv_song(local)) != NULL) {
        const char *song_track = mpd_song_get_tag(song, MPD_TAG_TRACK, 0);
        const char *song_title = mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
        const char *song_uri = mpd_song_get_uri(song);

        char song_string[500] = "";

        if (strlen(song_track) == 1)
            snprintf(song_string, 500, "0%s - %s", song_track, song_title);
        else
            snprintf(song_string, 500, "%s - %s", song_track, song_title);

        songs_browser->add(song_string, (void *)song_uri);
    }

    if (song)
        mpd_song_free(song);

    songs_browser->value(1);

    // Add album to queue
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        mpd_search_add_db_songs(local, true);

        mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_ALBUM, album);
        mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_DATE, year);

        mpd_search_commit(local);

    } else {
        songs_browser->do_callback();

        if (display_images)
            get_images((const char *)songs_browser->data(songs_browser->value()));
    }

    if (year) {
        free(year);

        year = NULL;
    }

    if (album) {
        free(album);

        album = NULL;
    }

    if (local)
        mpd_connection_free(local);
}

// ADD SONG TO QUEUE
void add_song(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    int song_line = songs_browser->value();

    if (!song_line)
        return;

    const char *song_uri = ((const char *)songs_browser->data(song_line));

    // Add song to queue
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        local = mpd_connection_new(host, port, 0);

        mpd_run_add(local, song_uri);
    }

    if (local)
        mpd_connection_free(local);
}

// GET PLAYLISTS
void get_lists(void) {
    struct mpd_connection *local = NULL;

    struct mpd_entity *playlist_entity = NULL;

    lists_browser->clear();

    local = mpd_connection_new(host, port, 0);

    mpd_send_list_playlists(local);

    while ((playlist_entity = mpd_recv_entity(local)) != NULL) {
        struct mpd_playlist *playlist = NULL;

        if (mpd_entity_get_type(playlist_entity) == MPD_ENTITY_TYPE_PLAYLIST) {
            playlist = (struct mpd_playlist *)mpd_entity_get_playlist(playlist_entity);

            const char *playlist_path = mpd_playlist_get_path(playlist);

            lists_browser->add(playlist_path);

            if (playlist)
                mpd_playlist_free(playlist);
        }
    }

    if (lists_browser->size() > 0) {
        lists_browser->sort(FL_SORT_ASCENDING);

        button_rename->activate();
        button_rename->redraw();

        button_delete->activate();
        button_delete->redraw();
    }

    if (playlist_entity)
        mpd_entity_free(playlist_entity);

    if (local)
        mpd_connection_free(local);
}

// GET PLAYLIST SONGS
void get_list_songs(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    struct mpd_entity *playlist_entity = NULL;

    int list_line = lists_browser->value();

    if (!list_line) {
        button_rename->deactivate();
        button_rename->redraw();

        button_delete->deactivate();
        button_delete->redraw();

        return;
    }

    button_rename->activate();
    button_rename->redraw();

    button_delete->activate();
    button_delete->redraw();

    list_songs_browser->clear();

    local = mpd_connection_new(host, port, 0);

    mpd_send_list_playlist(local, lists_browser->text(list_line));

    while ((playlist_entity = mpd_recv_entity(local)) != NULL) {
        if (mpd_entity_get_type(playlist_entity) == MPD_ENTITY_TYPE_SONG) {
            const struct mpd_song *song = mpd_entity_get_song(playlist_entity);

            const char *song_uri = mpd_song_get_uri(song);

            list_songs_browser->add(song_uri);

            if (song)
                mpd_song_free((struct mpd_song *)song);
        }
    }

    if (playlist_entity)
        mpd_entity_free(playlist_entity);

    list_songs_browser->value(1);

    // Add list to queue
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        mpd_run_load(local, lists_browser->text(list_line));
    }

    if (local)
        mpd_connection_free(local);
}

// ADD PLAYLIST SONG TO QUEUE
void add_list_song(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    int list_song_line = list_songs_browser->value();

    if (!list_song_line)
        return;

    // Add list song to queue
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        local = mpd_connection_new(host, port, 0);

        mpd_run_add(local, list_songs_browser->text(list_song_line));
    }

    if (local)
        mpd_connection_free(local);
}

// SAVE PLAYLIST
void save_list(Fl_Widget *, void *) {
    MPDC_Input_Window *iw = new MPDC_Input_Window(
        40 * 11,
        40 * 3,
        lc.save_new_playlist
    );
    iw->button_no_label(lc.cancel);
    iw->button_yes_label(lc.accept);
    iw->textsize(font_size);

    while (iw->visible()) {
        Fl::wait();
    }

    if (iw->value() == 1) {
        // See if that name already exists
        for (int l = 1; l <= lists_browser->size(); l++) {
            if (strcmp(lists_browser->text(l), iw->text()) == 0) {
                MPDC_Info_Window *infw = new MPDC_Info_Window(
                    40 * 11,
                    40 * 3,
                    lc.save_new_playlist
                );
                infw->button_close_label(lc.close);
                infw->textsize(font_size);
                infw->action_message(lc.playlist_exists);

                while (infw->visible()) {
                    Fl::wait();
                }

                return;
            }
        }

        mpd_run_save(connection, iw->text());

        button_rename->activate();
        button_rename->redraw();

        button_delete->activate();
        button_delete->redraw();

        lists_browser->add(iw->text());
        lists_browser->sort(FL_SORT_ASCENDING);
        lists_browser->redraw();
        lists_browser->value(1);
        lists_browser->do_callback();
    }
}

// RENAME PLAYLIST
void rename_list(Fl_Widget *, void *) {
    int line = lists_browser->value();

    MPDC_Input_Window *iw = new MPDC_Input_Window(
        40 * 11,
        40 * 3,
        lc.ren
    );
    iw->button_no_label(lc.cancel);
    iw->button_yes_label(lc.accept);
    iw->textsize(font_size);
    iw->action_message(lists_browser->text(line));
    iw->enable_yes_button();

    while (iw->visible()) {
        Fl::wait();
    }

    if (iw->value() == 1) {
        mpd_run_rename(connection, lists_browser->text(line), iw->text());

        lists_browser->text(line, iw->text());
        lists_browser->redraw();
    }
}

// DELETE PLAYLIST
void delete_list(Fl_Widget *, void *) {
    MPDC_Confirm_Window *cw = new MPDC_Confirm_Window(
        40 * 11,
        40 * 2,
        lc.del
    );
    cw->button_no_label(lc.no);
    cw->button_yes_label(lc.yes);
    cw->textsize(font_size);
    cw->action_message(lc.del_confirm);

    while (cw->visible()) {
        Fl::wait();
    }

    if (cw->value() == 1) {
        int line = lists_browser->value();

        mpd_run_rm(connection, lists_browser->text(line));

        lists_browser->remove(line);
        lists_browser->redraw();

        if (lists_browser->size() > 0) {
            lists_browser->value(line == 1 ? 1 : line - 1);
            lists_browser->do_callback();

        } else {
            button_rename->deactivate();
            button_rename->redraw();

            button_delete->deactivate();
            button_delete->redraw();
        }
    }
}

// GET GENRES
void get_genres(void) {
    struct mpd_connection *local = NULL;

    struct mpd_pair *genre_pair = NULL;

    chosen_genre_1->clear();
    chosen_genre_2->clear();
    chosen_genre_3->clear();

    local = mpd_connection_new(host, port, 0);

    mpd_search_db_tags(local, MPD_TAG_GENRE);

    mpd_search_commit(local);

    while ((genre_pair = mpd_recv_pair_tag(local, MPD_TAG_GENRE)) != NULL) {
        chosen_genre_1->add(genre_pair->value);
        chosen_genre_2->add(genre_pair->value);
        chosen_genre_3->add(genre_pair->value);

        mpd_return_pair(local, genre_pair);
    }

    if (local)
        mpd_connection_free(local);
}

// GET ALBUMS BY CHOSEN GENRES
void get_genres_albums(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    struct mpd_song *song = NULL;

    int album_line = 1;

    genres_albums_browser->clear();
    genres_songs_browser->clear();

    local = mpd_connection_new(host, port, 0);

    mpd_search_db_songs(local, true);

    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_GENRE, chosen_genre_1->value());
    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_GENRE, chosen_genre_2->value());
    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_GENRE, chosen_genre_3->value());

    mpd_search_add_sort_tag(local, MPD_TAG_DATE, false);

    mpd_search_commit(local);

    while ((song = mpd_recv_song(local)) != NULL) {
        const char *album = mpd_song_get_tag(song, MPD_TAG_ALBUM, 0);
        const char *year = mpd_song_get_tag(song, MPD_TAG_DATE, 0);

        const char *prev_album = genres_albums_browser->text(album_line);

        char song_string[500] = "";

        if (prev_album == NULL) {
            snprintf(song_string, 500, "%s - %s", year, album);

            genres_albums_browser->add(song_string);

        } else if (strcmp(prev_album + 7, album)) {
            snprintf(song_string, 500, "%s - %s", year, album);

            genres_albums_browser->add(song_string);

            album_line++;
        }
    }

    if (song)
        mpd_song_free(song);

    if (local)
        mpd_connection_free(local);

    genres_albums_browser->value(1);
    genres_albums_browser->do_callback();
}

// GET SONGS BY A GENRE ALBUM
void get_genres_songs(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    char *year = NULL;
    char *album = NULL;

    struct mpd_song *song = NULL;

    int i;
    int album_line = genres_albums_browser->value();

    if (!album_line)
        return;

    genres_songs_browser->clear();

    const char *year_and_album = genres_albums_browser->text(album_line);

    // Get year
    for (i = 0; ; i++) {
        if (year_and_album[i] == ' ' || year_and_album[i] == '\0')
            break;
    }

    year = strndup(year_and_album, i);

    // Get album
    album = strdup(year_and_album + i + 3);

    // Get songs
    local = mpd_connection_new(host, port, 0);

    mpd_search_db_songs(local, true);

    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_ALBUM, album);
    mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_DATE, year);

    mpd_search_add_sort_tag(local, MPD_TAG_DATE, false);

    mpd_search_commit(local);

    while ((song = mpd_recv_song(local)) != NULL) {
        const char *song_track = mpd_song_get_tag(song, MPD_TAG_TRACK, 0);
        const char *song_title = mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
        const char *song_uri = mpd_song_get_uri(song);

        char song_string[500] = "";

        if (strlen(song_track) == 1)
            snprintf(song_string, 500, "0%s - %s", song_track, song_title);
        else
            snprintf(song_string, 500, "%s - %s", song_track, song_title);

        genres_songs_browser->add(song_string, (void *)song_uri);
    }

    if (song)
        mpd_song_free(song);

    genres_songs_browser->value(1);

    // Add album to queue
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        mpd_search_add_db_songs(local, true);

        mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_ALBUM, album);
        mpd_search_add_tag_constraint(local, MPD_OPERATOR_DEFAULT, MPD_TAG_DATE, year);

        mpd_search_commit(local);

    } else {
        genres_songs_browser->do_callback();

        if (display_images)
            get_images((const char *)genres_songs_browser->data(genres_songs_browser->value()));
    }

    if (year) {
        free(year);

        year = NULL;
    }

    if (album) {
        free(album);

        album = NULL;
    }

    if (local)
        mpd_connection_free(local);
}

// ADD A SONG BY A GENRE ALBUM
void add_genre_song(Fl_Widget *, void *) {
    struct mpd_connection *local = NULL;

    int song_line = genres_songs_browser->value();

    if (!song_line)
        return;

    const char *song_uri = ((const char *)genres_songs_browser->data(song_line));

    // Add song to queue
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        local = mpd_connection_new(host, port, 0);

        mpd_run_add(local, song_uri);
    }

    if (local)
        mpd_connection_free(local);
}

// QUIT
void quit(Fl_Widget *, void *) {
    MPDC_Confirm_Window *cw = new MPDC_Confirm_Window(
        40 * 11,
        40 * 2,
        lc.exit
    );
    cw->action_message(lc.exit_confirm);
    cw->button_no_label(lc.no);
    cw->button_yes_label(lc.yes);
    cw->textsize(font_size);

    while (cw->visible()) {
        Fl::wait();
    }

    if (cw->value() == 1) {
        if (connection)
            mpd_connection_free(connection);

        Fl::hide_all_windows();
    }
}

// SCAN COMMAND LINE ARGUMENTS
int scan_args(int argc, char **argv, int &i) {
    if (strcmp("-h", argv[i]) == 0 || strcmp("--help", argv[i]) == 0) {
        help_flag = 1;

        i += 1;

        return 1;
    }

    if (strcmp("-D", argv[i]) == 0 || strcmp("--directory", argv[i]) == 0) {
        if (i < argc - 1 && argv[i + 1] != 0) {
            music_directory = argv[i + 1];

            i += 2;

            return 2;
        }
    }

    if (strcmp("-H", argv[i]) == 0 || strcmp("--host", argv[i]) == 0) {
        if (i < argc - 1 && argv[i + 1] != 0) {
            host = argv[i + 1];

            i += 2;

            return 2;
        }
    }

    if (strcmp("-S", argv[i]) == 0 || strcmp("--scheme", argv[i]) == 0) {
        if (i < argc - 1 && argv[i + 1] != 0) {
            scheme = argv[i + 1];

            i += 2;

            return 2;
        }
    }

    if (strcmp("-P", argv[i]) == 0 || strcmp("--port", argv[i]) == 0) {
        if (i < argc - 1 && argv[i + 1] != 0) {
            port = atoi(argv[i + 1]);

            i += 2;

            return 2;
        }
    }

    if (strcmp("-c", argv[i]) == 0 || strcmp("--crossfade", argv[i]) == 0) {
        if (i < argc - 1 && argv[i + 1] != 0) {
            crossfade_sec = atoi(argv[i + 1]);

            i += 2;

            return 2;
        }
    }

    if (strcmp("-t", argv[i]) == 0 || strcmp("--text-size", argv[i]) == 0) {
        if (i < argc - 1 && argv[i + 1] != 0) {
            font_size = atoi(argv[i + 1]);

            i += 2;

            return 2;
        }
    }

    if (strcmp("-n", argv[i]) == 0 || strcmp("--no-images", argv[i]) == 0) {
        display_images = false;

        i += 1;

        return 1;
    }

    if (strcmp("-C", argv[i]) == 0 || strcmp("--compact", argv[i]) == 0) {
        compact = true;

        i += 1;

        return 1;
    }

    if (strcmp("-o", argv[i]) == 0 || strcmp("--own-icons", argv[i]) == 0) {
        own_icons = true;

        i += 1;

        return 1;
    }

    return 0;
}

// CHECK IF IT'S A STREAM
bool is_stream(const char *song_uri) {
    if (strcasestr(song_uri, "http://") || strcasestr(song_uri, "https://"))
        return true;

    return false;
}

// GET IMAGE
void get_images(const char *song_uri) {
    if (compact) {
        Fl_Shared_Image *cover_img = NULL,
                        *tmp = NULL;

        int i, num_files;

        dirent **files = NULL;

        char path[FL_PATH_MAX] = "";
        char album_path[FL_PATH_MAX] = "";

        snprintf(path, FL_PATH_MAX, "%s%s", music_directory, song_uri);

        // Get cover artwork
        char *song_dir = dirname(path);

        snprintf(album_path, FL_PATH_MAX, "%s/", song_dir);

        num_files = fl_filename_list(album_path, &files, fl_alphasort);

        for (i = 0; i < num_files; i++) {
            if (strcasestr(files[i]->d_name, "cover.") ||
                strcasestr(files[i]->d_name, "front.") ||
                strcasestr(files[i]->d_name, "folder.")
               ) {
                strcat(album_path, files[i]->d_name);

                break;
            }
        }

        fl_filename_free_list(&files, num_files);

        // Free image
        tmp = ((Fl_Shared_Image *)images_box->image());

        if (tmp) {
            tmp->release();
            tmp = NULL;
        }

        // Get and scale image
        if (fl_filename_isdir(album_path) == 0)
            cover_img = Fl_Shared_Image::get(album_path);

        images_box->image(NULL);
        images_box->hide();
        images_box->show();
        images_box->redraw();

        if (cover_img) {
            int c_w = cover_img->w();
            int c_h = cover_img->h();
            int b_w = images_box->w();
            int b_h = images_box->h();

            float fw = c_w / float(b_w);
            float fh = c_w / float(b_h);
            float f = fw > fh ? fh : fw;

            c_w = int(c_w / f);
            c_h = int(c_h / f);

            cover_img->scale(c_w, c_h, 1, 1);

            images_box->image(cover_img);
            images_box->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
            images_box->redraw();
        }

    // Normal view
    } else {
        Fl_Shared_Image *logo_img = NULL,
                        *artist_img = NULL,
                        *cover_img = NULL,
                        *tmp = NULL;

        int i, num_files;

        dirent **files = NULL;

        char path[FL_PATH_MAX] = "";
        char artist_path[FL_PATH_MAX] = "";
        char logo_path[FL_PATH_MAX] = "";
        char album_path[FL_PATH_MAX] = "";

        snprintf(path, FL_PATH_MAX, "%s%s", music_directory, song_uri);

        // Get cover artwork
        char *song_dir = dirname(path);

        snprintf(album_path, FL_PATH_MAX, "%s/", song_dir);

        num_files = fl_filename_list(album_path, &files, fl_alphasort);

        for (i = 0; i < num_files; i++) {
            if (strcasestr(files[i]->d_name, "cover.") ||
                strcasestr(files[i]->d_name, "front.") ||
                strcasestr(files[i]->d_name, "folder.")
               ) {
                strcat(album_path, files[i]->d_name);

                break;
            }
        }

        fl_filename_free_list(&files, num_files);

        // Get logo and artist images
        char *artist_dir = dirname(song_dir);

        snprintf(logo_path, FL_PATH_MAX, "%s/", artist_dir);
        snprintf(artist_path, FL_PATH_MAX, "%s/", artist_dir);

        num_files = fl_filename_list(artist_path, &files, fl_alphasort);

        for (i = 0; i < num_files; i++) {
            if (strcasestr(files[i]->d_name, "artist.")) {
                strcat(artist_path, files[i]->d_name);
            }

            if (strcasestr(files[i]->d_name, "logo.")) {
                strcat(logo_path, files[i]->d_name);
            }
        }

        fl_filename_free_list(&files, num_files);

        // Free images
        tmp = ((Fl_Shared_Image *)logo_box->image());

        if (tmp) {
            tmp->release();
            tmp = NULL;
        }

        tmp = ((Fl_Shared_Image *)artist_box->image());

        if (tmp) {
            tmp->release();
            tmp = NULL;
        }

        tmp = ((Fl_Shared_Image *)cover_box->image());

        if (tmp) {
            tmp->release();
            tmp = NULL;
        }

        // Load images
        if (fl_filename_isdir(logo_path) == 0)
            logo_img = Fl_Shared_Image::get(logo_path);

        if (fl_filename_isdir(artist_path) == 0)
            artist_img = Fl_Shared_Image::get(artist_path);

        if (fl_filename_isdir(album_path) == 0)
            cover_img = Fl_Shared_Image::get(album_path);

        scale_and_show_images(logo_img, artist_img, cover_img);
    }
}

// SCALE AND SHOW IMAGE
void scale_and_show_images(Fl_Shared_Image *l, Fl_Shared_Image *a, Fl_Shared_Image *c) {
    int l_h = 0;
    int a_h = 0;
    int c_h = 0;
    int t_h = 0;

    // Reset boxes
    logo_box->resize(
        images_box->x(),
        images_box->y(),
        images_box->w(),
        images_box->h() / 3
    );
    logo_box->image(NULL);
    logo_box->hide();
    logo_box->show();
    logo_box->redraw();

    artist_box->resize(
        images_box->x(),
        images_box->y() + logo_box->h() - 1,
        images_box->w(),
        images_box->h() / 3
    );
    artist_box->image(NULL);
    artist_box->hide();
    artist_box->show();
    artist_box->redraw();

    cover_box->resize(
        images_box->x(),
        images_box->y() + logo_box->h() + artist_box->h() - 1,
        images_box->w(),
        images_box->h() - logo_box->h() - artist_box->h()
    );
    cover_box->image(NULL);
    cover_box->hide();
    cover_box->show();
    cover_box->redraw();

    // Set images
    if (l) {
        l->scale(logo_box->w(), main_window->h(), 1, 1);

        l_h = l->h();

        logo_box->image(l);
        logo_box->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        logo_box->resize(images_box->x(), images_box->y(), images_box->w(), l_h);
        logo_box->redraw();

    } else {
        logo_box->resize(images_box->x(), images_box->y(), images_box->w(), 0);
        logo_box->hide();
    }

    if (a) {
        a->scale(artist_box->w(), main_window->h(), 1, 1);

        a_h = a->h();

        artist_box->image(a);
        artist_box->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        artist_box->resize(images_box->x(), images_box->y() + logo_box->h(), images_box->w(), a_h);
        artist_box->redraw();

    } else {
        artist_box->resize(images_box->x(), images_box->y() + logo_box->h(), images_box->w(), 0);
        artist_box->hide();
    }

    if (c) {
        c->scale(cover_box->w(), main_window->h(), 1, 1);

        c_h = c->h();

        cover_box->image(c);
        cover_box->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        cover_box->resize(images_box->x(), images_box->y() + logo_box->h() + artist_box->h(), images_box->w(), c_h);
        cover_box->redraw();

    } else {
        cover_box->resize(images_box->x(), images_box->y() + logo_box->h() + artist_box->h(), images_box->w(), 0);
        cover_box->hide();
    }

    // Resize if the hight is too big
    t_h = l_h + a_h + c_h;

    if (t_h > images_box->h()) {
        while (t_h > images_box->h()) {
            if (l_h >= images_box->h() / 3)
                l_h -= 1;

            t_h = l_h + a_h + c_h;

            if (a_h >= images_box->h() / 3)
                a_h -= 1;

            t_h = l_h + a_h + c_h;

            if ((l_h < images_box->h() / 3) && (a_h < images_box->h() / 3))
                c_h -= 1;

            t_h = l_h + a_h + c_h;
        }

        logo_box->resize(images_box->x(), images_box->y(), images_box->w(), l_h);
        logo_box->redraw();

        artist_box->resize(images_box->x(), images_box->y() + logo_box->h() - 1, images_box->w(), a_h);
        artist_box->redraw();

        cover_box->resize(images_box->x(), images_box->y() + logo_box->h() + artist_box->h() - 1, images_box->w(), c_h);
        cover_box->redraw();
    }
}

// QUEUE CALLBACK
void queue_cb(Fl_Widget *, void *) {
    // Drag and drop
    if (Fl::event_button() == FL_LEFT_MOUSE) {
        user_grabbed_sel = false;

        int iv = queue_browser->initial_value();
        int dv = queue_browser->destination_value();

        if (Fl::event() == FL_RELEASE) {
            if (iv != dv)
                mpd_run_move(connection, iv - 1, dv - 1);

            user_grabbed_sel = false;
        }
    }

    // Popup menu
    if (Fl::event_button() == FL_RIGHT_MOUSE) {
        // Can be different from the playing track
        int line = queue_browser->value_under_mouse();

        if (!line) {
            user_grabbed_sel = false;

            return;
        }

        user_grabbed_sel = true;

        // Make sure the line is selected
        queue_browser->value(line);

        Fl_Menu_Button *menu = new Fl_Menu_Button(
            queue_browser->x(),
            queue_browser->y(),
            queue_browser->w(),
            queue_browser->h(),
            NULL
        );
        menu->type(Fl_Menu_Button::POPUP3);
        menu->textsize(font_size);
        menu->selection_color(FL_FOREGROUND_COLOR);
        menu->add(lc.play,        0, play,        NULL, FL_MENU_DIVIDER);
        menu->add(lc.move_up,     0, move_up,     NULL,               0);
        menu->add(lc.move_down,   0, move_down,   NULL, FL_MENU_DIVIDER);
        menu->add(lc.remove_item, 0, remove_item, NULL,               0);
        menu->popup();
    }
}

// PLAY PREVIOUS TRACK
void play_prev(Fl_Widget *, void *) {
    mpd_run_previous(connection);
}

// PLAY NEXT TRACK
void play_next(Fl_Widget *, void *) {
    mpd_run_next(connection);
}

// PLAY
void play(Fl_Widget *, void *) {
    int line_num = queue_browser->value();

    if (!line_num)
        mpd_run_play(connection);
    else
        mpd_run_play_pos(connection, line_num - 1);

    user_grabbed_sel = false;
}

// TOGGLE PAUSE
void toggle_pause(Fl_Widget *, void *) {
    struct mpd_status *status = NULL;

    enum mpd_state state;

    mpd_run_toggle_pause(connection);

    status = mpd_status_begin();
    status = mpd_run_status(connection);

    state = mpd_status_get_state(status);

    if (state == MPD_STATE_PAUSE) {
        button_pause->image(px_paused);
    } else {
        button_pause->image(px_pause);
    }

    button_pause->redraw();

    if (status)
        mpd_status_free(status);
}

// STOP
void stop(Fl_Widget *, void *) {
    mpd_run_stop(connection);
}

// CLEAR QUEUE
void clear_queue(Fl_Widget *, void *) {
    mpd_run_stop(connection);

    queue_browser->clear();

    button_save->deactivate();
    button_save->redraw();

    mpd_run_clear(connection);
}

// SHUFFLE QUEUE
void shuffle(Fl_Widget *, void *) {
    mpd_run_shuffle(connection);
}

// TOGGLE CROSSFADE
void crossfade(Fl_Widget *, void *) {
    crossfade_on = !crossfade_on;

    if (crossfade_on) {
        mpd_run_crossfade(connection, crossfade_sec);

        button_crossfade->image(px_crossfade_on);

    } else {
        mpd_run_crossfade(connection, 0);

        button_crossfade->image(px_crossfade);
    }

    button_crossfade->redraw();
}

// SET CROSSFADE
void set_crossfade(Fl_Widget *, void *) {
    crossfade_sec = input_crossfade->value();

    if (crossfade_on)
        mpd_run_crossfade(connection, crossfade_sec);
}

// TOGGLE RANDOM
void random(Fl_Widget *, void *) {
    random_on = !random_on;

    mpd_run_random(connection, random_on);

    if (random_on) {
        button_random->image(px_random_on);
    } else {
        button_random->image(px_random);
    }

    button_random->redraw();
}

// TOGGLE REPEAT
void repeat(Fl_Widget *, void *) {
    repeat_on = !repeat_on;

    mpd_run_repeat(connection, repeat_on);

    if (repeat_on) {
        button_repeat->image(px_repeat_on);
    } else {
        button_repeat->image(px_repeat);
    }

    button_repeat->redraw();
}

// MOVE QUEUE ITEM DOWN
void move_down(Fl_Widget *, void *) {
    int line_num = queue_browser->value();

    mpd_run_move(connection, line_num - 1, line_num);

    user_grabbed_sel = false;
}

// MOVE QUEUE ITEM UP
void move_up(Fl_Widget *, void *) {
    int line_num = queue_browser->value();

    mpd_run_move(connection, line_num - 1, line_num - 2);

    user_grabbed_sel = false;
}

// REMOVE QUEUE ITEM
void remove_item(Fl_Widget *, void *) {
    int line_num = queue_browser->value();

    mpd_run_delete(connection, line_num - 1);

    user_grabbed_sel = false;
}

// SHOW LYRICS
void show_lyrics(const char *song_uri) {
    lyrics_buffer->remove_modify_callback(save_lyrics, NULL);
    lyrics_buffer->text(NULL);

    snprintf(full_path, FL_PATH_MAX, "%s%s", music_directory, song_uri);

    fl_filename_setext(full_path, sizeof(full_path), ".txt");

    if (lyrics_buffer->loadfile(full_path) != 0) {
        lyrics_buffer->savefile(full_path);
    }

    lyrics_buffer->add_modify_callback(save_lyrics, NULL);
}

// SAVE LYRICS
void save_lyrics(int, int nInserted, int nDeleted, int, const char *, void *) {
    if (nInserted || nDeleted) {
        lyrics_buffer->savefile(full_path);
    }
}

// SEARCH
void search(Fl_Widget *, void *) {
    pthread_t thread;

    button_search->deactivate();
    button_search->redraw();

    pthread_create(&thread, NULL, search_thread, NULL);
}

// START SEARCH THREAD
void *search_thread(void *) {
    struct mpd_connection *local = NULL;

    struct mpd_song *song = NULL;

    const char *item = search_input->value();

    static int prev_artist_line = 0;
    static int prev_album_line = 0;
    static int prev_song_line = 0;
    static int counter = 0;

    int artist_line;
    int album_line;
    int song_line;

    bool found = false;

    // Do search
    Fl::lock();

    local = mpd_connection_new(host, port, 0);

    mpd_search_db_songs(local, false);

    mpd_search_add_any_tag_constraint(local, MPD_OPERATOR_DEFAULT, item);

    mpd_search_add_sort_tag(local, MPD_TAG_ARTIST, false);

    mpd_search_commit(local);

    while ((song = mpd_recv_song(local)) != NULL) {
        const char *song_artist = mpd_song_get_tag(song, MPD_TAG_ARTIST, 0);
        const char *song_album = mpd_song_get_tag(song, MPD_TAG_ALBUM, 0);
        const char *song_title = mpd_song_get_tag(song, MPD_TAG_TITLE, 0);

        if (!found) {
            // Find the artist
            for (artist_line = 1; artist_line <= artists_browser->size(); artist_line++) {
                if (!strcmp(artists_browser->text(artist_line), song_artist)) {
                    if (prev_artist_line < artist_line) {
                        prev_artist_line = artist_line;

                        prev_album_line = 0;

                        artists_browser->show(prev_artist_line);
                        artists_browser->value(prev_artist_line);
                        artists_browser->do_callback();

                        found = true;

                        break;
                    }
                }
            }

            // Find the album
            if (strcasestr(song_album, item) || strcasestr(song_title, item))
            for (album_line = 1; album_line <= albums_browser->size(); album_line++) {
                if (strcasestr(albums_browser->text(album_line), song_album)) {
                    if (prev_album_line < album_line) {
                        prev_album_line = album_line;

                        prev_song_line = 0;

                        albums_browser->show(prev_album_line);
                        albums_browser->value(prev_album_line);
                        albums_browser->do_callback();

                        found = true;

                        break;
                    }
                }
            }

            // Find the song
            if (strcasestr(song_title, item))
            for (song_line = 1; song_line <= songs_browser->size(); song_line++) {
                if (strcasestr(songs_browser->text(song_line), song_title)) {
                    if (prev_song_line < song_line) {
                        prev_song_line = song_line;

                        songs_browser->show(prev_song_line);
                        songs_browser->value(prev_song_line);
                        songs_browser->do_callback();

                        found = true;

                        break;
                    }
                }
            }

        } else {
            mpd_search_cancel(local);
        }
    }

    // Start over
    if (artist_line > artists_browser->size() &&
        album_line > albums_browser->size() &&
        song_line > songs_browser->size()
       ) {
        counter++;

        if (counter >= 1) {
            prev_artist_line = 0;

            counter = 0;
        }
    }

    button_search->activate();
    button_search->redraw();

    Fl::unlock();

    Fl::awake();

    if (song)
        mpd_song_free(song);

    if (local)
        mpd_connection_free(local);

    return NULL;
}

// ACTIVATE OR DEACTIVATE THE SEARCH BUTTON
void search_input_check(Fl_Widget *, void *) {
    if (search_input->size() > 0)
        button_search->activate();
    else
        button_search->deactivate();

    button_search->redraw();
}

// SEEK FORWARD OR BACKWARD
void rewind(Fl_Widget *, void *) {
    struct mpd_status *status = NULL;

    struct mpd_song *current_song = NULL;

    enum mpd_state state = MPD_STATE_UNKNOWN;

    status = mpd_status_begin();
    status = mpd_run_status(connection);

    state = mpd_status_get_state(status);

    if (state == MPD_STATE_PLAY || state == MPD_STATE_PAUSE) {
        current_song = mpd_run_current_song(connection);

        const char *song_uri = mpd_song_get_uri(current_song);

        unsigned song_pos = mpd_status_get_song_pos(status);
        unsigned total_time = mpd_status_get_total_time(status);

        if (!is_stream(song_uri)) {
            double val = prog_bar->value();

            int sec = int(val * total_time);

            mpd_run_seek_pos(connection, song_pos, sec);

        } else {
            prog_bar->value(1);
        }

    } else {
        prog_bar->value(0);
    }

    if (current_song)
        mpd_song_free(current_song);

    if (status)
        mpd_status_free(status);
}

// UPDATE MUSIC DATABASE
void update(Fl_Widget *, void *) {
    MPDC_Confirm_Window *cw = new MPDC_Confirm_Window(
        40 * 11,
        40 * 2,
        lc.update
    );
    cw->button_no_label(lc.no);
    cw->button_yes_label(lc.yes);
    cw->textsize(font_size);
    cw->action_message(lc.update_message);

    while (cw->visible()) {
        Fl::wait();
    }

    if (cw->value() == 1) {
        pthread_t thread;

        button_update->deactivate();
        button_update->redraw();

        pthread_create(&thread, NULL, update_thread, NULL);
    }
}

// START UPDATE THREAD
void *update_thread(void *) {
    struct mpd_connection *local = NULL;

    bool result = false;

    Fl::lock();

    local = mpd_connection_new(host, port, 0);

    mpd_send_update(local, NULL);

	mpd_recv_update_id(local);

	result = mpd_response_finish(local);

    if (result) {
        get_genres();

        chosen_genre_1->value(0);
        chosen_genre_2->value(0);
        chosen_genre_3->value(0);
        chosen_genre_3->do_callback();

        get_artists();

        artists_browser->value(1);
        artists_browser->do_callback();

        fill_queue();

        button_update->activate();
        button_update->redraw();
    }

    Fl::unlock();

    Fl::awake(update_thread_end, NULL);

    if (local)
        mpd_connection_free(local);

    return NULL;
}

// SHOW UPDATE THREAD WINDOW
void update_thread_end(void *) {
    MPDC_Info_Window *infw = new MPDC_Info_Window(
        40 * 11,
        40 * 3,
        lc.update
    );
    infw->button_close_label(lc.close);
    infw->textsize(font_size);
    infw->action_message(lc.update_complete);
}

// GET ICON THEME
void get_icon_theme(void) {
    static char *line = NULL;

	static size_t line_size = 0;

	char path[FL_PATH_MAX] = "";

	char *home = getenv("HOME");

	snprintf(path, FL_PATH_MAX, "%s/.config/gtk-3.0/settings.ini", home);

	FILE *settings = fopen(path, "r");

	if (!settings)
	    return;

	while ((getline(&line, &line_size, settings)) > 0) {
        if (strstr(line, "gtk-icon-theme-name=")) {
            sscanf(line, "gtk-icon-theme-name=%s", icon_theme);

            sprintf(icon_theme_orig, icon_theme);

            break;
        }
    }

    if (line) {
        free(line);

        line = NULL;
    }

    fclose(settings);
}

// GET ICON
Fl_Image *get_icon(const char *type, const char *name) {
    Fl_Image *icon = NULL;

    int i, num_files;

    dirent **files;

    char path[FL_PATH_MAX] = "";

start_over:
    // Global icon
    snprintf(path, FL_PATH_MAX, "/user/share/icons/%s/%s/24/", icon_theme, type);

    num_files = fl_filename_list(path, &files, fl_alphasort);

    for (i = 0; i < num_files; i++) {
        if (!strncmp(files[i]->d_name, name, strlen(name))) {
            strcat(path, files[i]->d_name);

            sprintf(icon_theme, icon_theme_orig);

            fl_filename_free_list(&files, num_files);

            const char *ext = fl_filename_ext(path);

            if (!strcasecmp(ext, ".svg"))
                icon = new Fl_SVG_Image(path);
            else
                icon = new Fl_PNG_Image(path);

            return icon;
        }
    }

    // User icon
    char *home = fl_getenv("HOME");

    snprintf(path, FL_PATH_MAX, "%s/.local/share/icons/%s/%s/24/", home, icon_theme, type);

    num_files = fl_filename_list(path, &files, fl_alphasort);

    for (i = 0; i < num_files; i++) {
        if (!strncmp(files[i]->d_name, name, strlen(name))) {
            strcat(path, files[i]->d_name);

            sprintf(icon_theme, icon_theme_orig);

            fl_filename_free_list(&files, num_files);

            const char *ext = fl_filename_ext(path);

            if (!strcasecmp(ext, ".svg"))
                icon = new Fl_SVG_Image(path);
            else
                icon = new Fl_PNG_Image(path);

            return icon;
        }
    }

    // Get inherited theme
    size_t len = 0;

    ssize_t read;

    char *line;

    FILE *index;

    sprintf(icon_theme_orig, icon_theme);

    snprintf(path, FL_PATH_MAX, "/user/share/icons/%s/index.theme", icon_theme);

    index = fopen(path, "r");

    if (!index) {
        snprintf(path, FL_PATH_MAX, "%s/.local/share/icons/%s/index.theme", home, icon_theme);

        index = fopen(path, "r");
    }

    while ((read = getline(&line, &len, index)) != -1) {
        if (strstr(line, "Inherits=")) {
            sscanf(line, "Inherits=%s", icon_theme);

            break;
        }
    }

    fclose(index);

    if (line) {
        free(line);

        line = NULL;
    }

    goto start_over;

    return icon;
}
