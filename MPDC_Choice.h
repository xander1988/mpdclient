/*
 * MPDClient - A Music Player Daemon client
 * Copyright (C) 2025 Xander

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Menu_Button.H>

class MPDC_Choice: public Fl_Group {
    int value_;
    int prev_value_;

    Fl_Box *box;

    Fl_Output *output;

    Fl_Button *button;

    Fl_Menu_Button *list;

    // Static shit
    static void static_popup_list(Fl_Widget *, void *data) {
        ((MPDC_Choice *)data)->popup_list();
    }

    static void static_set_value(Fl_Widget *, void *data) {
        ((MPDC_Choice *)data)->set_value();
    }

    // Normal callbacks
    void popup_list(void) {
        list->popup();
    }

    void set_value(void) {
        const char *text = list->text();

        output->value(text);
    }

    // Handle events
    int handle(int event) {
        switch (event) {
        case FL_UNFOCUS:
            if (when() & FL_WHEN_RELEASE) {
                button->value(0);

                value_ = list->value();

                if (prev_value_ != value_) {
                    prev_value_ = value_;

                    if (prev_value_ >= 0) {
                        do_callback();
                    }
                }
            }

            break;

        default:
            break;
        }

        return Fl_Group::handle(event);
    }

public:
    // Constructor
    MPDC_Choice(int x, int y, int w, int h, const char *l = NULL): Fl_Group(x, y, w, h, l) {
        value_ = -1;
        prev_value_ = -1;

        box = new Fl_Box(
            FL_DOWN_BOX,
            x,
            y,
            w,
            h,
            NULL
        );

        output = new Fl_Output(
            box->x() + 2,
            box->y() + 2,
            box->w() - 28 - 4,
            box->h() - 4
        );
        output->box(FL_FLAT_BOX);
        output->color(FL_BACKGROUND2_COLOR);

        button = new Fl_Button(
            box->x() + box->w() - (28 + 2),
            box->y() + 2,
            28,
            box->h() - 4,
            "@+12>"
        );
        button->type(FL_TOGGLE_BUTTON);
        button->callback(static_popup_list, (void *)this);

        list = new Fl_Menu_Button(
            box->x(),
            box->y(),
            box->w(),
            box->h(),
            NULL
        );
        list->selection_color(FL_FOREGROUND_COLOR);
        list->callback(static_set_value, (void *)this);
        list->hide();

        resizable(output);
        end();
    }

    // Clear the list
    void clear(void) {
        list->clear();
    }
    
    // Set current text
    void value(int pos) {
        const char *text = list->text(pos);

        output->value(text);
    }

    // Get current text
    const char *value(void) const {
        return output->value();
    }

    // Add text to list
    void add(const char *item) {
        list->add(item, 0, NULL, NULL, 0);
    }

    // Set font
    void textfont(Fl_Font ft) {
        output->textfont(ft);

        list->textfont(ft);
    }

    // Set font size
    void textsize(Fl_Fontsize sz) {
        output->textsize(sz);

        list->textsize(sz);
    }
};
