# FMPDC

FMPDC is a Music Player Daemon client written in FLTK. The main difference from most other clients is that FMPDC can search in the database combining up to 3 genre tags via menus, so the resulting list contains only those albums which include songs with selected tags. For example, if a song titled "Some_song" has Pop;Italy;80s in its genre field, you can select Pop in the first menu, Italy in the second, and 80s in the third, and the album having that song will be found. Songs in MPD's database have to be extensively tagged for this to work, of course.

FMPDC can show 3 artist-related pictures from top to bottom on its main page: artist's logo, artist's photo, and a cover of an album being played. The first two files must have "logo" and "artist" in their names, and must reside in artist's main directory. The cover art lies in an album dir and should be named either cover, or folder, or front. All this is case-insensitive.

FMPDC can show song lyrics as well. As of now, it does not fetch lyrics from the Web (I personally don't like it when software makes frequent connections to the Web; and also some other clients have very poor fetching techniques, resulting in wrong or no lyrics being downloaded for a song), so you'll have to manually copy a text from somewhere and paste to the page. Lyrics are saved just upon being copied to or edited, and the corresponding file is named like its song file with the .txt extension. Lyrics are saved to album directories.

## Requirements
You'll need FLTK 1.4.1 of higher and libmpd along with the usuall buil tools.

## Configuration
The configuration is done via command line arguments, run FMPDC with the -h flag to read.
