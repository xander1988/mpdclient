/*
 * MPDClient - A Music Player Daemon client
 * Copyright (C) 2025 Xander and others

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <FL/Fl.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Menu_Window.H>

// Floating tip window
class TipWin: public Fl_Menu_Window {
    char tip[40];

    Fl_Color text_color;

    Fl_Fontsize text_size;

    void draw(void) {
        Fl_Color c = fl_color_cube(FL_NUM_RED - 1, FL_NUM_GREEN - 1, FL_NUM_BLUE - 2);

        draw_box(FL_BORDER_BOX, 0, 0, w(), h(), c);

        fl_color(text_color);

        fl_font(labelfont(), text_size);

        fl_draw(tip, 3, 3, w() - 6, h() - 6, Fl_Align(FL_ALIGN_LEFT | FL_ALIGN_WRAP));
    }

public:
    // will autosize
    TipWin(): Fl_Menu_Window(1, 1) {
        strcpy(tip, "XX:XX");

        text_color = FL_FOREGROUND_COLOR;

        text_size = labelsize();

        set_override();

        end();
    }

    void value(int d) {
        sprintf(tip, "%02d:%02d", d % 3600 / 60, d % 60);

        // Recalc size of window
        fl_font(labelfont(), text_size);

        int W = w(), H = h();

        fl_measure(tip, W, H, 0);

        W += 8;

        size(W, H);

        redraw();
    }

    void textcolor(Fl_Color c) {
        text_color = c;
    }

    void textsize(Fl_Fontsize s) {
        text_size = s;
    }
};

// Progress bar
class MPDC_Prog_Bar: public Fl_Slider {
public:
    TipWin *timetip;

protected:
    unsigned total_time;

    void draw_bg(int X, int Y, int W, int H) {
        fl_push_clip(X, Y, W, H);

        draw_box();

        fl_pop_clip();
    }

    void draw(void) {
        if (damage() & FL_DAMAGE_ALL)
            draw_box();

        draw(
            x() + Fl::box_dx(box()),
            y() + Fl::box_dy(box()),
            w() - Fl::box_dw(box()),
            h() - Fl::box_dh(box())
        );
    }

    void draw(int x, int y, int w, int h) {
        double val;

        if (minimum() == maximum())
            val = 0.5;

        else {
            val = (value() - minimum()) / (maximum() - minimum());

            if (val > 1.0)
                val = 1.0;
            else if (val < 0.0)
                val = 0.0;
        }

        int ww = w;

        int xx, S;

        S = int(val * ww + .5);

        if (minimum() > maximum()) {
            S = ww - S;
            xx = ww - S;

        } else xx = 0;

        int xsl, ysl, wsl, hsl;

        xsl = x + xx;
        wsl = S;
        ysl = y;
        hsl = h;

        draw_bg(x, y, w, h);

        if (wsl > 0 && hsl > 0)
            draw_box(FL_FLAT_BOX, xsl, ysl, wsl, hsl, FL_SELECTION_COLOR);

        draw_label(xsl, ysl, wsl, hsl);
    }

    int handle(int e) {
        int val = 0;
        int mx;
        int wd;

        switch(e) {
        case FL_ENTER:
            if (total_time > 0) {
                // XXX: if offscreen, move tip ABOVE mouse instead
                timetip->position(Fl::event_x_root(), Fl::event_y_root() + 30);
                timetip->value(val);
                timetip->show();
            }

            break;

        case FL_MOVE:
            if (total_time > 0) {
                wd = w() - 4;
                mx = Fl::event_x() - x() - 2;

                if (mx < 0)
                    mx = 0;

                if (mx > wd)
                    mx = wd;

                val = int((mx * total_time) / wd);

                timetip->position(Fl::event_x_root(), Fl::event_y_root() + 30);
                timetip->value(val);

            } else {
                timetip->hide();
            }

            break;

        // Valuator goes away
        case FL_HIDE:

        // Leave focus
        case FL_LEAVE:
            // Make sure tipwin closes when app closes
            timetip->hide();

            break;
        }

        return Fl_Slider::handle(e);
    }

public:
    MPDC_Prog_Bar(int x, int y, int w, int h, const char *l): Fl_Slider(x, y, w, h, l) {
        type(FL_HOR_FILL_SLIDER);

        total_time = 0;

        // Save current widget...
        Fl_Group *save = Fl_Group::current();

        // ...because this trashes it
        timetip = new TipWin();
        timetip->hide();

        Fl_Group::current(save);
    }

    void get_song_time(unsigned time) {
        total_time = time;
    }
};
