/*
 * MPDClient - A Music Player Daemon client
 * Copyright (C) 2025 Xander and others

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <FL/Fl.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>

// Demonstrate draggable Fl_Browser item reordering - erco@seriss.com 07/01/16
class MPDC_DND_Browser: public Fl_Browser {
    void *dragitem;
    void *dstitem;

    int was_moved;
    int iv;
    int dv;

    void change_cursor(Fl_Cursor newcursor) {
        fl_cursor(newcursor, FL_BLACK, FL_WHITE);
    }

    void *item_under_mouse() {
        int X, Y, W, H;

        bbox(X, Y, W, H);

        if (!Fl::event_inside(X, Y, W, H))
            return 0;

        return find_item(Fl::event_y());
    }

    int handle(int event) {
        if (event == FL_PUSH) {
            int modifiers = FL_SHIFT | FL_CTRL | FL_ALT | FL_META;

            if (Fl::event_button() == 1 && ((Fl::event_state() & modifiers) == 0)) {
                if ((dragitem = item_under_mouse())) {
                    Fl_Browser_::select_only(dragitem, 0);

                    change_cursor(FL_CURSOR_HAND);

                    was_moved = 0;

                    iv = lineno(dragitem);
                    dv = iv;

                    return 1;
                }
            }
        }

        if (event == FL_DRAG) {
            if (dragitem) {
                dstitem = item_under_mouse();

                if (dstitem != dragitem) {
                    item_swap(dragitem, dstitem);

                    Fl_Browser_::select(dragitem, 1, 0);

                    was_moved = 1;

                    redraw();
                }

                return 1;
            }
        }

        if (event == FL_RELEASE) {
            if (dragitem) {
                change_cursor(FL_CURSOR_DEFAULT);

                if (was_moved) {
                    dv = lineno(dstitem);

                    Fl_Browser_::select(dragitem, 0, 0);

                    redraw();
                }

                dragitem = NULL;
                dstitem = NULL;

                was_moved = 0;
            }
        }

        return Fl_Browser::handle(event);
    }

public:
    MPDC_DND_Browser(int X, int Y, int W, int H): Fl_Browser(X, Y, W, H) {
        dragitem = NULL;
        dstitem = NULL;

        was_moved = 0;
        iv = 0;
        dv = 0;
    }

    int value_under_mouse(void) {
        int val = 0;

        val = lineno(item_under_mouse());

        return val;
    }

    int initial_value(void) const {
        return iv;
    }

    int destination_value(void) const {
        return dv;
    }

    ~MPDC_DND_Browser() {
    }
};

// Demonstrate how to derive a class extending Fl_Browser with interactively resizable columns
// erco 1.10 12/09/2005
// erco 1.20 07/17/2016 -- fix scrollbar recalc, code simplifications
class MPDC_Browser: public Fl_Group {
public:
    MPDC_DND_Browser *browser;

protected:
    Fl_Box *main_box;

    Fl_Cursor _last_cursor;

    Fl_Fontsize header_ts;

    int _drag_col;
    int *_widths;
    int _nowidths[1];
    int columns;
    int old_value;

    char line_style[2];

    const char **headers;

    void change_cursor(Fl_Cursor newcursor) {
        if (newcursor == _last_cursor)
            return;

        window()->cursor(newcursor);

        _last_cursor = newcursor;
    }

    int which_col_near_mouse() {
        int xx = browser->x();
        int yy = main_box->y() + 2;
        int ww = browser->w();
        int hh = main_box->h() - 4;
        int hsbar_h = browser->hscrollbar.h();
        int vsbar_w = browser->scrollbar.w();

        if (browser->hscrollbar.visible() == 0)
            hsbar_h = 0;

        if (browser->scrollbar.visible() == 0)
            vsbar_w = 0;

        if (!Fl::event_inside(xx, yy, ww - vsbar_w, hh - hsbar_h))
            return -1;

        int mousex = Fl::event_x() + browser->hposition();
        int colx = browser->x();

        for (int t = 0; t < columns - 1; t++) {
            colx += _widths[t];

            int diff = mousex - colx;

            if (diff >= -4 && diff <= 4) {
                return t;
            }
        }

        return -1;
    }

    void recalc_hscroll() {
        int size = browser->textsize();

        browser->textsize(size + 1);
        browser->textsize(size);

        redraw();
    }

    int handle(int e) {
        int ret = 0;

        if (e == FL_ENTER) {
            ret = 1;
        }

        if (e == FL_MOVE) {
            change_cursor((which_col_near_mouse() >= 0) ? FL_CURSOR_WE : FL_CURSOR_DEFAULT);

            ret = 1;
        }

        if (e == FL_PUSH) {
            int whichcol = which_col_near_mouse();

            if (whichcol >= 0) {
                _drag_col = whichcol;

                return 1;

            } else {
                int xx = browser->x();
                int yy = browser->y();
                int ww = browser->w();
                int hh = browser->h();
                int sbar_h = browser->hscrollbar.h();

                if (browser->hscrollbar.visible() == 0)
                    sbar_h = 0;

                if (Fl::event_inside(xx, yy, ww, hh - sbar_h))
                    do_callback();
            }

            redraw();
        }

        if (e == FL_DRAG) {
            if (_drag_col != -1) {
                int mousex = Fl::event_x() + browser->hposition();
                int newwidth = mousex - browser->x();

                for (int t = 0; _widths[t] && t < _drag_col; t++) {
                    newwidth -= _widths[t];
                }

                if (newwidth > 0) {
                    _widths[_drag_col] = newwidth;

                    if (_widths[_drag_col] < 2) {
                        _widths[_drag_col] = 2;
                    }

                    recalc_hscroll();
                }

                return 1;
            }
        }

        if (e == FL_LEAVE || e == FL_RELEASE) {
            _drag_col = -1;

            change_cursor(FL_CURSOR_DEFAULT);

            redraw();

            ret = 1;
        }

        int _value = browser->hscrollbar.value();

        if (old_value != _value) {
            old_value = _value;

            redraw();
        }

        return (Fl_Group::handle(e) ? 1 : ret);
    }

    void draw() {
        // Draw group
        Fl_Group::draw();

        // Draw column separators
        int colx = browser->x() - browser->hposition();
        int hsbar_h = browser->hscrollbar.h();
        int vsbar_x = browser->scrollbar.x();
        int vsbar_w = browser->scrollbar.w();
        int t;

        if (browser->hscrollbar.visible() == 0)
            hsbar_h = 1;

        if (browser->scrollbar.visible() == 0)
            vsbar_x = vsbar_w = 0;

        fl_push_clip(browser->x(), browser->y() - 36, browser->w(), browser->h() + 36);

        for (t = 0; t < columns - 1; t++) {
            // Draw header
            fl_color(FL_BACKGROUND_COLOR);

            fl_line_style(0);

            if (!t)
                fl_draw_box(FL_UP_BOX, colx, browser->y() - 36, _widths[t] + 1, 36, color());
            else
                fl_draw_box(FL_UP_BOX, colx + 1, browser->y() - 36, _widths[t], 36, color());

            // Draw label
            Fl_Label l;
            l.align_ = FL_ALIGN_LEFT | FL_ALIGN_INSIDE;
            l.color = FL_FOREGROUND_COLOR;
            l.font = 0;
            l.value = headers[t];
            l.size = header_ts;
            l.type = 0;
            l.image = NULL;
            l.deimage = NULL;
            l.h_margin_ = 0;
            l.v_margin_ = 0;
            l.spacing = 0;
            l.draw(colx + 4, browser->y() - 36, _widths[t], 36, FL_ALIGN_LEFT | FL_ALIGN_INSIDE);

            // Draw column separator
            colx += _widths[t];

            if (colx > browser->x() && colx < (browser->x() + browser->w())) {
                fl_color(FL_FOREGROUND_COLOR);

                fl_line_style(FL_DOT, 1, line_style);

                if (colx < vsbar_x || colx > vsbar_x + vsbar_w)
                    fl_line(colx, browser->y() + 1, colx, browser->y() + browser->h() - hsbar_h);
            }
        }

        // The closing header box
        int width = 0;
        int sum = 0;

        for (int e = 0; e < columns - 1; e++)
            sum += _widths[e];

        width = (browser->w() - (sum - browser->hposition())) - 1;

        fl_color(FL_BACKGROUND_COLOR);

        fl_line_style(0);

        fl_draw_box(FL_UP_BOX, colx + 1, browser->y() - 36, width, 36, color());

        Fl_Label l;
        l.align_ = FL_ALIGN_LEFT | FL_ALIGN_INSIDE;
        l.color = FL_FOREGROUND_COLOR;
        l.font = 0;
        l.value = headers[t];
        l.size = header_ts;
        l.type = 0;
        l.image = NULL;
        l.deimage = NULL;
        l.h_margin_ = 0;
        l.v_margin_ = 0;
        l.spacing = 0;
        l.draw(colx + 4, browser->y() - 36, _widths[t], 36, FL_ALIGN_LEFT | FL_ALIGN_INSIDE);

        fl_pop_clip();
    }

public:
    MPDC_Browser(int X, int Y, int W, int H, const char *l) : Fl_Group(X, Y, W, H, l) {
        _last_cursor  = FL_CURSOR_DEFAULT;
        _drag_col     = -1;
        _nowidths[0]  = 0;
        _widths       = _nowidths;
        line_style[0] = 2;
        line_style[1] = 2;
        columns       = 0;
        header_ts     = 14;
        old_value     = 0;

        headers = NULL;

        main_box = new Fl_Box(
            FL_DOWN_BOX,
            X,
            Y,
            W,
            H,
            NULL
        );

        browser = new MPDC_DND_Browser(
            main_box->x() + 2,
            main_box->y() + 2 + 36,
            main_box->w() - 4,
            main_box->h() - (4 + 36)
        );
        browser->box(FL_FLAT_BOX);
        browser->color(FL_BACKGROUND2_COLOR);
        browser->type(FL_HOLD_BROWSER);

        end();
        resizable(browser);
    }

    int *column_widths() const {
        return _widths;
    }

    void column_widths(int *val) {
        _widths = val;

        browser->column_widths(val);
    }

    void column_headers(const char **val) {
        headers = val;
    }

    void column_char(char val) {
        browser->column_char(val);
    }

    void total_columns(int val) {
        columns = val;
    }

    void add(const char *val) {
        browser->add(val);
    }

    void clear() {
        browser->clear();
    }

    void text_size(Fl_Fontsize val) {
        header_ts = val;

        browser->textsize(val);
    }

    const char *text(int val) const {
        return browser->text(val);
    }

    int value() const {
        return browser->value();
    }

    void value(int val) {
        browser->value(val);
    }

    void select(int val) {
        browser->select(val);
    }

    void deselect() {
        browser->deselect();
    }

    int value_under_mouse() const {
        return browser->value_under_mouse();
    }

    int initial_value() const {
        return browser->initial_value();
    }

    int destination_value() const {
        return browser->destination_value();
    }

    ~MPDC_Browser() {
    }
};
