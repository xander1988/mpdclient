/*
 * MPDClient - A Music Player Daemon client
 * Copyright (C) 2025 Xander

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Text_Display.H>

class MPDC_Info_Window: public Fl_Double_Window {
    Fl_Text_Buffer *info;

    Fl_Text_Display *text_display;

    Fl_Button *button_close;

protected:
    // Static shit
    static void s_done(Fl_Widget *, void *data) {
        ((MPDC_Info_Window *)data)->done();
    }

    // Normal callback
    void done(void) {
        hide();
    }

public:
    // Constructor
    MPDC_Info_Window(int w, int h, const char *title): Fl_Double_Window(w, h, title) {
        Fl::visual(FL_DOUBLE | FL_INDEX);

        // Text
        Fl_Group *group_action_text = new Fl_Group(
            0,
            0,
            w,
            h - 36,
            NULL
        );

        info = new Fl_Text_Buffer();

        text_display = new Fl_Text_Display(
            group_action_text->x() + 4,
            group_action_text->y() + 4,
            group_action_text->w() - 8,
            group_action_text->h() - 8,
            NULL
        );
        text_display->buffer(info);
        text_display->wrap_mode(Fl_Text_Display::WRAP_AT_BOUNDS, 0);
        text_display->color(FL_BACKGROUND_COLOR);

        group_action_text->resizable(text_display);
        group_action_text->end();

         // Just close
        Fl_Group *group_action_button_close = new Fl_Group(
            0,
            group_action_text->y() + group_action_text->h(),
            w,
            h - group_action_text->h(),
            NULL
        );

        Fl_Box *button_close_box_left = new Fl_Box(
            FL_NO_BOX,
            group_action_button_close->x(),
            group_action_button_close->y(),
            group_action_button_close->w() / 2 - ((28 * 3) / 2),
            group_action_button_close->h(),
            NULL
        );

        button_close = new Fl_Button(
            button_close_box_left->x() + button_close_box_left->w() + 4,
            button_close_box_left->y() + 4,
            28 * 3,
            button_close_box_left->h() - 8,
            NULL
        );
        button_close->align(FL_ALIGN_INSIDE | FL_ALIGN_CLIP);
        button_close->callback(s_done, (void *)this);

        new Fl_Box(
            FL_NO_BOX,
            button_close->x() + button_close->w() + 4,
            group_action_button_close->y(),
            group_action_button_close->w() - button_close_box_left->w() - button_close->w() - 8,
            group_action_button_close->h(),
            NULL
        );

        group_action_button_close->resizable(button_close);
        group_action_button_close->end();

        // End
        resizable(text_display);
        end();
        set_modal();
        show();
    }

    // Set message
    void action_message(const char *m) {
        info->text(m);
    }

    // Set buttons' labels
    void button_close_label(const char *l) {
        button_close->label(l);
    }

    // Set font
    void textfont(Fl_Font ft) {
        text_display->textfont(ft);

        button_close->labelfont(ft);
    }

    // Set font size
    void textsize(Fl_Fontsize sz) {
        text_display->textsize(sz);

        button_close->labelsize(sz);
    }

    // Destroy
    ~MPDC_Info_Window() {
    }
};
