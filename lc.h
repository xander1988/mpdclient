/*
 * MPDClient - A Music Player Daemon client
 * Copyright (C) 2025 Xander

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include <string.h>
#include <locale.h>

struct Localize {
    // Translatable strings
    const char *artwork;
    const char *artists;
    const char *genres;
    const char *lists;
    const char *lyrics;
    const char *queue;

    const char *prev;
    const char *next;
    const char *play;
    const char *pause;
    const char *stop;

    const char *shuffle;
    const char *crossfade;
    const char *random;
    const char *repeat;

    const char *save;
    const char *clear;

    const char *move_down;
    const char *move_up;
    const char *remove_item;

    const char *kbps;
    const char *bits;
    const char *sample_rate;
    const char *mono;
    const char *stereo;

    const char *items;
    const char *hours;
    const char *minutes;
    const char *seconds;

    const char *blank;

    const char *search;

    const char *save_new_playlist;
    const char *playlist_exists;
    const char *ren;
    const char *del;
    const char *del_confirm;
    const char *accept;
    const char *yes;
    const char *no;
    const char *close;
    const char *cancel;

    const char *exit;
    const char *exit_confirm;

    const char *update;
    const char *update_message;
    const char *update_complete;

    const char *error;
    const char *warning;
    const char *music_dir_not_set;

    const char *about;

    const char *track;
    const char *title;
    const char *duration;
    const char *year;
    const char *artist;
    const char *album;
};

extern struct Localize lc;

void do_localize(struct Localize *);
