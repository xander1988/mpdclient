/*
 * MPDClient - A Music Player Daemon client
 * Copyright (C) 2025 Xander

 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

#include "lc.h"

struct Localize lc;

void do_localize(struct Localize *localization) {
    int i = 0;

    char lang[10] = "";

    char *locale = setlocale(LC_ALL, "");

    while (i <= 9) {
        if (locale[i] == '.')
            break;

        lang[i] = locale[i];

        i++;
    }

    // Russian
    if (strcmp(lang, "ru_RU") == 0) {
        localization->artwork = "Обложка";
        localization->artists = "Исполнители";
        localization->genres = "Стили";
        localization->lists = "Списки";
        localization->lyrics = "Текст";
        localization->queue = "Очередь";

        localization->prev = "Предыдущая композиция";
        localization->next = "Следующая композиция";
        localization->play = "Играть";
        localization->pause = "Пауза";
        localization->stop = "Стоп";

        localization->shuffle = "Перемешать очередь";
        localization->crossfade = "Перекрестное затухание";
        localization->random = "Случайная композиция";
        localization->repeat = "Повтор";

        localization->save = "Сохранить список";
        localization->clear = "Очистить очередь";

        localization->move_down = "Переместить ниже";
        localization->move_up = "Переместить выше";
        localization->remove_item = "Удалить из очереди";

        localization->kbps = "кбит/с";
        localization->bits = "бит";
        localization->sample_rate = "Гц";
        localization->mono = "моно";
        localization->stereo = "стерео";

        localization->items = "элем.";
        localization->hours = "час.";
        localization->minutes = "мин.";
        localization->seconds = "сек.";

        localization->blank = "<пусто>";

        localization->search = "Искать";

        localization->save_new_playlist = "Сохранить новый список";
        localization->playlist_exists = "Список с этим названием уже существует";
        localization->ren = "Переименовать список";
        localization->del = "Удалить список";
        localization->del_confirm = "Хотите удалить список?";
        localization->accept = "Принять";
        localization->yes = "Да";
        localization->no = "Нет";
        localization->close = "Закрыть";
        localization->cancel = "Отмена";

        localization->exit = "Выход из MPDClient";
        localization->exit_confirm = "Хотите выйти?";

        localization->update = "Обновить базу";
        localization->update_message = "Хотите обновить базу?";
        localization->update_complete = "Обновление завершено";

        localization->error = "Ошибка";
        localization->warning = "Предупреждение";
        localization->music_dir_not_set = "Вы не указали корневой каталог с музыкой. Установите его опцией '-D' или '--directory' (в конце должен быть знак '/'), чтобы FMPDC мог показывать обложки альбомов, фотографии и тексты песен.";

        localization->about = "FLTK Music Daemon Client\n\nКод частично взят из Erco's FLTK Cheat Page: https://www.seriss.com/people/erco/fltk/";

        localization->track = "Дорожка";
        localization->title = "Название";
        localization->duration = "Длительность";
        localization->year = "Год";
        localization->artist = "Исполнитель";
        localization->album = "Альбом";

    // Default: en_US
    } else {
        localization->artwork = "Artwork";
        localization->artists = "Artists";
        localization->genres = "Genres";
        localization->lists = "Lists";
        localization->lyrics = "Lyrics";
        localization->queue = "Queue";

        localization->prev = "Previous track";
        localization->next = "Next track";
        localization->play = "Play";
        localization->pause = "Toggle pause";
        localization->stop = "Stop";

        localization->shuffle = "Shuffle queue";
        localization->crossfade = "Crossfade";
        localization->random = "Random track";
        localization->repeat = "Repeat";

        localization->save = "Save playlist";
        localization->clear = "Clear queue";

        localization->move_down = "Move down";
        localization->move_up = "Move up";
        localization->remove_item = "Remove from queue";

        localization->kbps = "kbps";
        localization->bits = "bits";
        localization->sample_rate = "Hz";
        localization->mono = "mono";
        localization->stereo = "stereo";

        localization->items = "item(s)";
        localization->hours = "hour(s)";
        localization->minutes = "minute(s)";
        localization->seconds = "second(s)";

        localization->blank = "<empty>";

        localization->search = "Search";

        localization->save_new_playlist = "Save new playlist";
        localization->playlist_exists = "Playlist with that name already exists";
        localization->ren = "Rename list";
        localization->del = "Delete list";
        localization->del_confirm = "Do you want to delete the list?";
        localization->accept = "Accept";
        localization->yes = "Yes";
        localization->no = "No";
        localization->close = "Close";
        localization->cancel = "Cancel";

        localization->exit = "Quit MPDClient";
        localization->exit_confirm = "Do you want to quit?";

        localization->update = "Update database";
        localization->update_message = "Do you want to update database?";
        localization->update_complete = "Update complete";

        localization->error = "Error";
        localization->warning = "Warning";
        localization->music_dir_not_set = "You did not set the root music directory. Use the -D or --directory option to set it (must end with a slash) so that FMPDC be able to display artwork and song lyrics.";

        localization->about = "FLTK Music Daemon Client\n\nParts of the code taken from Erco's FLTK Cheat Page: https://www.seriss.com/people/erco/fltk/";

        localization->track = "Track";
        localization->title = "Title";
        localization->duration = "Duration";
        localization->year = "Year";
        localization->artist = "Artist";
        localization->album = "Album";
    }
}
